#include <stdio.h>
#include <time.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif
	
	typedef uint64_t u64;
	
#define PROFILER_STACK_SIZE 64
	
	typedef struct
	{
		const char *data;
		int len;
	} str;
	
	typedef struct
	{
		str name;
		double secondsElapsed;
		u64 hits;
	} timed_section;
	
	
	typedef struct
	{
		str name;
		u64 timestamp;
		int recordIndex;
	} record;
	
	timed_section TimedSections[PROFILER_STACK_SIZE] = {0};
	record Records[PROFILER_STACK_SIZE] = {0};
	record *ActiveRecords[PROFILER_STACK_SIZE] = {0};
	int ProfilerStackPointer = 0;
	int ActiveProfilers = 0;
	
#if defined __linux__
	// This Linux code has last been tested in 2023-01
	u64 PerformanceCounter()
	{
		struct timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		
		return ts.tv_nsec + (u64)ts.tv_sec * (u64)1e9;
	}
	
	double PerformanceFrequency()
	{
		return 1e9;
	}
#elif defined _WIN32
#ifndef WIN32_LEAN_AND_MEAN
  #define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
	// This Windows code has last been tested in 2023-01
	u64 PerformanceCounter()
	{
		LARGE_INTEGER Counter;
		QueryPerformanceCounter(&Counter);
		
		return Counter.QuadPart;
	}
	
	double PerformanceFrequency()
	{
		static int Initialized = 0;
		static double Frequency;
		if(!Initialized)
		{
			LARGE_INTEGER I;
			Initialized = 1;
			QueryPerformanceFrequency(&I);
			Frequency = (double)I.QuadPart;
		}
		return Frequency;
	}
#else
#error "Performance counters for this OS are not implemented yet!"
#endif /* __linux__, _WIN32 */
	
	
	void ProfileBegin(const char *name)
	{
		assert(name != NULL);
		
		// Todo: Move this to a function
		int recordIndex = -1;
		int namelen = strnlen(name, 255);
		int i = 0;
		for(;i < PROFILER_STACK_SIZE; i++)
		{
			record *R = &Records[i];
			if(!R->name.data)
			{
				R->name.data = name;
				R->name.len = namelen;
				recordIndex = i;
				ActiveProfilers++;
				break;
			}
			else
			{
				if(namelen != R->name.len)
				{
					continue;
				}
				
				int n = 0;
				while(n < namelen && name[n] == R->name.data[n])
				{
					n++;
				}
				if(n == namelen) 
				{
					recordIndex = i;
					break;
				}
			}
		}
		assert(recordIndex != -1);
		assert(i < PROFILER_STACK_SIZE);
		assert(ProfilerStackPointer < PROFILER_STACK_SIZE);
		
		ActiveRecords[ProfilerStackPointer] = &Records[recordIndex];
		record *Record = ActiveRecords[ProfilerStackPointer];
		
		Record->recordIndex = recordIndex;
		Record->timestamp = PerformanceCounter();
		
		ProfilerStackPointer++;
	}
	
	void ProfileEnd()
	{
		assert(ProfilerStackPointer > 0);
		
		record *Record = ActiveRecords[--ProfilerStackPointer];
		u64 now = PerformanceCounter();
		u64 elapsed = now - Record->timestamp;
		int sectionIndex = Record->recordIndex;
		double seconds = (double)elapsed / PerformanceFrequency();
		
		TimedSections[sectionIndex].hits++;
		TimedSections[sectionIndex].secondsElapsed += seconds;
		TimedSections[sectionIndex].name = Record->name;
	}
	
	void ProfileClear()
	{
		for(int i = 0; i < ActiveProfilers; i++)
		{
			timed_section *Ts = &TimedSections[i];
			Ts->secondsElapsed = 0;
			Ts->hits = 0;
		}
	}
	
	void ProfileReport()
	{
		double totalMs = 0;
		printf("===================================================\n");
		for(int i = 0; i < ActiveProfilers; i++)
		{
			timed_section *Ts = &TimedSections[i];
			double ms = (Ts->secondsElapsed / (double)Ts->hits) * 1000.0;
			totalMs += Ts->secondsElapsed;
			printf("%-15s %.4fms (%.4fs total)\n", Ts->name.data, ms, Ts->secondsElapsed);
		}
		
		totalMs *= 1000.0;
		printf("Total: %.4f ms\n", totalMs);
		
		printf("===================================================\n");
	}
	
#ifdef __cplusplus
}
#endif

#if 0
// Demo
#if defined __linux__
#include <unistd.h>
void Sleep_MS(unsigned int milliseconds)
{
	usleep(milliseconds * 1000);
}
#elif defined _WIN32
void Sleep_MS(unsigned int milliseconds)
{
	Sleep(milliseconds);
}
#else
#error "Sleep_MS is not implemented for this OS yet"
#endif // __linux__, _WIN32

int main()
{
	unsigned int milliseconds = 123;
	
	ProfileBegin("Sleep Total");
	
	// Sequential
	ProfileBegin("Sleep1");
	Sleep_MS(milliseconds);
	ProfileEnd();
	
	ProfileBegin("Sleep2");
	Sleep_MS(milliseconds);
	ProfileEnd();
	
	
	// Using the stack system
	ProfileBegin("Level0");
	Sleep_MS(15);
	ProfileBegin("Level1");
	Sleep_MS(20);
	ProfileEnd(); // Level1
	ProfileEnd(); // Level0
	
	
	ProfileEnd(); // Sleep Total
	
	ProfileReport();
}

#endif // 1 (demo)
