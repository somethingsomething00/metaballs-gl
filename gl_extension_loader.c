// Generated with gen.py

#ifdef __cplusplus
extern "C" {
#endif

/************************
* Includes
************************/
#include <stdio.h>

/************************
* Typedefs
************************/
typedef void (APIENTRY *pfn_glLinkProgram) (GLuint program);
typedef void (APIENTRY *pfn_glAttachShader) (GLuint program, GLuint shader);
typedef void (APIENTRY *pfn_glShaderSource) (GLuint shader, GLsizei count, const GLchar *const*string, const GLint *length);
typedef void (APIENTRY *pfn_glUseProgram) (GLuint program);
typedef void (APIENTRY *pfn_glUniform1f) (GLint location, GLfloat v0);
typedef void (APIENTRY *pfn_glUniform2f) (GLint location, GLfloat v0, GLfloat v1);
typedef void (APIENTRY *pfn_glUniform1fv) (GLint location, GLsizei count, const GLfloat *value);
typedef void (APIENTRY *pfn_glUniform2fv) (GLint location, GLsizei count, const GLfloat *value);
typedef void (APIENTRY *pfn_glUniform1i) (GLint location, GLint v0);
typedef GLint (APIENTRY *pfn_glGetUniformLocation) (GLuint program, const GLchar *name);
typedef void (APIENTRY *pfn_glCompileShader) (GLuint shader);
typedef GLuint (APIENTRY *pfn_glCreateProgram) (void);
typedef GLuint (APIENTRY *pfn_glCreateShader) (GLenum type);
typedef void (APIENTRY *pfn_glDeleteProgram) (GLuint program);
typedef void (APIENTRY *pfn_glDeleteShader) (GLuint shader);
typedef void (APIENTRY *pfn_glDetachShader) (GLuint program, GLuint shader);
typedef void (APIENTRY *pfn_glGetProgramiv) (GLuint program, GLenum pname, GLint *params);
typedef void (APIENTRY *pfn_glGetProgramInfoLog) (GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
typedef void (APIENTRY *pfn_glGetShaderiv) (GLuint shader, GLenum pname, GLint *params);
typedef void (APIENTRY *pfn_glGetShaderInfoLog) (GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
typedef void (APIENTRY *pfn_glGetShaderSource) (GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *source);

/************************
* Decls
************************/
pfn_glLinkProgram glLinkProgram;
pfn_glAttachShader glAttachShader;
pfn_glShaderSource glShaderSource;
pfn_glUseProgram glUseProgram;
pfn_glUniform1f glUniform1f;
pfn_glUniform2f glUniform2f;
pfn_glUniform1fv glUniform1fv;
pfn_glUniform2fv glUniform2fv;
pfn_glUniform1i glUniform1i;
pfn_glGetUniformLocation glGetUniformLocation;
pfn_glCompileShader glCompileShader;
pfn_glCreateProgram glCreateProgram;
pfn_glCreateShader glCreateShader;
pfn_glDeleteProgram glDeleteProgram;
pfn_glDeleteShader glDeleteShader;
pfn_glDetachShader glDetachShader;
pfn_glGetProgramiv glGetProgramiv;
pfn_glGetProgramInfoLog glGetProgramInfoLog;
pfn_glGetShaderiv glGetShaderiv;
pfn_glGetShaderInfoLog glGetShaderInfoLog;
pfn_glGetShaderSource glGetShaderSource;

/************************
* Loader
************************/
typedef void *(*pfn_gl_get_proc_address)(const char *);
int GL_LoadExtensions (pfn_gl_get_proc_address gl_get_proc_address)
{
#define LOAD_GL_FUNCTION(n) do {\
    n = (pfn_##n)gl_get_proc_address(#n);\
    if(!n) {\
      fprintf(stderr, "ERROR: Could not load OpenGL function `%s`", #n);\
      success = 0;\
    }\
}while(0)

    int success = 1;

    LOAD_GL_FUNCTION(glLinkProgram);
    LOAD_GL_FUNCTION(glAttachShader);
    LOAD_GL_FUNCTION(glShaderSource);
    LOAD_GL_FUNCTION(glUseProgram);
    LOAD_GL_FUNCTION(glUniform1f);
    LOAD_GL_FUNCTION(glUniform2f);
    LOAD_GL_FUNCTION(glUniform1fv);
    LOAD_GL_FUNCTION(glUniform2fv);
    LOAD_GL_FUNCTION(glUniform1i);
    LOAD_GL_FUNCTION(glGetUniformLocation);
    LOAD_GL_FUNCTION(glCompileShader);
    LOAD_GL_FUNCTION(glCreateProgram);
    LOAD_GL_FUNCTION(glCreateShader);
    LOAD_GL_FUNCTION(glDeleteProgram);
    LOAD_GL_FUNCTION(glDeleteShader);
    LOAD_GL_FUNCTION(glDetachShader);
    LOAD_GL_FUNCTION(glGetProgramiv);
    LOAD_GL_FUNCTION(glGetProgramInfoLog);
    LOAD_GL_FUNCTION(glGetShaderiv);
    LOAD_GL_FUNCTION(glGetShaderInfoLog);
    LOAD_GL_FUNCTION(glGetShaderSource);

#undef LOAD_GL_FUNCTION

    return success;
}

#ifdef __cplusplus
}
#endif
