// Get cpu usage for the current program running
// It'd be nice if SDL3 got a feature like this :)


// Linux and Windows are supported

// Seconds
#define POLL_FREQUENCY 0.5 

#ifdef __linux__
#include <sys/resource.h>
#include <time.h>



// time_t      tv_sec;  /* Seconds */
// suseconds_t tv_usec; /* Microseconds */
float TimevalToSeconds(struct timeval *t)
{
	float s = t->tv_sec;
	float us = t->tv_usec / 1e6;
	
	return s + us;
}

// Not sure if we should be tracking both, or just user time
float RusageToSeconds(struct rusage *r)
{
	float t_user = TimevalToSeconds(&r->ru_utime);
	float t_sys = TimevalToSeconds(&r->ru_stime);
	
	return t_user + t_sys;
}

void SYS_Init()
{
	// Nothing to do for linux
}

float SYS_ProcessorUsage(double ElapsedSeconds)
{
	static int once = 1;
	static double accumulator = 0; // seconds
	static struct rusage last_usage;
	static float percentage = 0;
	
	if(once)
	{
		once = 0;
		getrusage(RUSAGE_SELF, &last_usage);
	}
	
	accumulator += ElapsedSeconds;
	if(accumulator >= POLL_FREQUENCY)
	{
		// Convert timeval to seconds
		// Subtract deltas
		// Get percentage of user and system time
		
		struct rusage current_usage;
		getrusage(RUSAGE_SELF, &current_usage);
		
		float t1 = RusageToSeconds(&last_usage);
		float t2 = RusageToSeconds(&current_usage);
		
		float delta = t2 - t1;
		percentage = (delta / accumulator) * 100.0;
		
		accumulator = 0;
		last_usage = current_usage;
	}
	return percentage;
}

#endif /* __linux__ */


#ifdef _WIN32

// Here's the same functionality for Windows

#include <windows.h>

typedef struct
{
	HANDLE CurrentProcess;
	int CoreCount;
} process_info;

static FILETIME KernelTimePrev, KernelTimeCurr;
static FILETIME UserTimePrev, UserTimeCurr;
static process_info ProcessInfo;
typedef HANDLE PROCESS_HANDLE;

PROCESS_HANDLE SYS_GetCurrentProcess()
{
	return GetCurrentProcess();
}

int SYS_GetProcessorCount()
{
	SYSTEM_INFO Si = {0};
	GetSystemInfo(&Si);
	return Si.dwNumberOfProcessors;
}

void SYS_Init()
{
	ProcessInfo.CurrentProcess = SYS_GetCurrentProcess();
	ProcessInfo.CoreCount = SYS_GetProcessorCount();
}

struct rusage
{
	FILETIME UserTime;
	FILETIME KernelTime;
};

// We could make this call GetCurrentProcess every time
// but for this one I'll cache it...
#define RUSAGE_SELF ProcessInfo.CurrentProcess

void getrusage(HANDLE Process, struct rusage *r)
{
	FILETIME CreationTime, ExitTime;
	GetProcessTimes(Process, 
					&CreationTime,
					&ExitTime,
					&r->KernelTime,
					&r->UserTime);
}

LARGE_INTEGER Win32FiletimeToLargeInteger(FILETIME *Ft)
{
	LARGE_INTEGER Li;
	Li.LowPart = Ft->dwLowDateTime;
	Li.HighPart = Ft->dwHighDateTime;
	
	return Li;
}

// According to MSDN, each unit of FILETIME is equivalent to 100 nanoseconds
// The input is expected to already have been combined to a 64 bit value
float Win32FiletimeUnitsToSeconds(LONGLONG Filetime)
{
	return (float)Filetime / 1e7;
}

float RusageToSeconds(struct rusage *r)
{
	LARGE_INTEGER t_user, t_kernel;
	
	t_user = Win32FiletimeToLargeInteger(&r->UserTime);
	t_kernel = Win32FiletimeToLargeInteger(&r->KernelTime);
	
	LONGLONG sum = t_user.QuadPart + t_kernel.QuadPart;
	
	float seconds = Win32FiletimeUnitsToSeconds(sum);
	
	return seconds;
}

// Copypaste from the Linux version
// I thought we may have to change it, but for now it's the same
// Just had to write some wrapper functions to emulate the Linux API
float SYS_ProcessorUsage(double ElapsedSeconds)
{
	static int once = 1;
	static double accumulator = 0; // seconds
	static struct rusage last_usage;
	static float percentage = 0;
	
	if(once)
	{
		once = 0;
		getrusage(ProcessInfo.CurrentProcess, &last_usage);
	}
	
	accumulator += ElapsedSeconds;
	if(accumulator >= POLL_FREQUENCY)
	{
		// Convert timeval to seconds
		// Subtract deltas
		// Get percentage of user and system time
		
		struct rusage current_usage;
		getrusage(RUSAGE_SELF, &current_usage);
		
		float t1 = RusageToSeconds(&last_usage);
		float t2 = RusageToSeconds(&current_usage);
		
		float delta = t2 - t1;
		percentage = (delta / accumulator) * 100.0;
		
		accumulator = 0;
		last_usage = current_usage;
	}
	
	// Note for Win32:
	// The processor times are combined for all cores
	// This means that if 4 cores are maxed out, you will see 400% cpu usage.
	// If this behaviour doesn't suit your needs, divide this result by ProcessInfo.CoreCount
	return percentage;
	
	//return percentage / ProcessInfo.CoreCount;
}

#endif /* _WIN32 */
