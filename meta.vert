// Allows use of immediate mode to render the shaders
#version 130

void main()
{
	vec4 pos = gl_Vertex;
	gl_Position = pos;
}
