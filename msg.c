#include <stdio.h>
#include <string.h>


// Simple message system
// Depends on console.c

typedef struct
{
	char text[256];
	int len;
	float lifetime;
} timed_message;

// This can be expanded, but for the metaball program, only 1 message makes sense
#define MSG_COUNT 1

static int m_MessagesUsed = 0;
static timed_message m_Messages[MSG_COUNT];

static timed_message *_MSG_GetNextFreeMessage()
{
	if(m_MessagesUsed >= MSG_COUNT) m_MessagesUsed = 0;
	return &m_Messages[m_MessagesUsed++];
}



void MSG_Send(const char *s, ...)
{
	va_list vl;

	va_start(vl, s);

	timed_message *m = _MSG_GetNextFreeMessage();
	if(!m) return;
	int written = vsnprintf(m->text, sizeof(m->text), s, vl);
	m->len = written;

	va_end(vl);

	m->lifetime = 3;
}

// This will show the most recent messages at the bottom
void MSG_Update(float ElapsedSeconds)
{
	int WorkingMessageCount = m_MessagesUsed;

	CON_BeginAt(2, 2);
	CON_SetColor(1, 1, 1, 1);

	for(int i = 0; i < MSG_COUNT; i++)
	{
		timed_message *m = &m_Messages[i];
		if(m->lifetime > 0)
		{

			m->lifetime -= ElapsedSeconds;

			CON_Print(m->text, m->len);

			if(m->lifetime <= 0)
			{
				WorkingMessageCount--;
				if(WorkingMessageCount < 0) WorkingMessageCount = 0;
			}
		}
	}
	m_MessagesUsed = WorkingMessageCount;
}
