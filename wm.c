// Light wrapper around SDL_SetWindowFullscreen

// Why does this file even exist? Well...
// For some reason, a fullscreen OpenGL window will use 100% of a single cpu core on my system. 
// This seems to be some driver issue with NVidia.
// Even if creating an OpenGL window using the Win32 API with wgl and GDI, this still happens.
// A fullscreen D3D window does not behave like this.

// I have found a workaround by setting native window style on Win32 to the following flag
// NewStyle = OldSyle & ~(WS_OVERLAPPEDWINDOW)

// Many applications also use WS_POPUP for their fullscreen window flags, but for me, this is what causes the issue.
// SDL itself uses WS_POPUP, so it's overriden here in WIN32_SetWindowFullscreen.

// One issue with this hack specific to SDL, is that the mouse position will  not always be consistent between resizes.
// I'm guessing SDL is doing some internal checks based on the window style, and this is throwing it off.
// However, I much prefer a small inconsistency like that compared to burning my cpu, 
// but if the mouse issue is problematic for the program you're writing, don't include this file.

#if defined (USE_WIN32_WINDOW_STYLE_HACK) && defined (_WIN32)

#include <windows.h>
#include <SDL2/SDL_syswm.h>

typedef struct
{
	HWND Window;
	DWORD InitialStyle;
} wm_info;

static wm_info WmInfo;

void WM_Init(SDL_Window *Window)
{
	SDL_SysWMinfo Info = {0};
	SDL_GetWindowWMInfo(Window, &Info);
	
	WmInfo.Window = Info.info.win.window;
	WmInfo.InitialStyle = GetWindowLong(WmInfo.Window, GWL_STYLE);
}

// When enable is anything but 0, I'm assuming we want a fullscreen window
// This corresponds to the way SDL_SetWindowFullscreen is called, except I'm not checking for flags
// Only the window style is modified here to be suitable for a fullscreen window
void WIN32_SetWindowFullscreen(SDL_Window *Window, int enable)
{
	if(enable)
	{
		// Don't rely on the initial HWND being valid, in case SDL reallocated it under the hood
		// Get a new info struct for every call
		SDL_SysWMinfo Info = {0};
		SDL_GetWindowWMInfo(Window, &Info);
		WmInfo.Window = Info.info.win.window;

		// Always use the cached style as a reference
		HWND Wnd = WmInfo.Window;
		DWORD ExistingStyle = WmInfo.InitialStyle;
		
		DWORD NewStyle = ExistingStyle & ~(WS_OVERLAPPEDWINDOW);
		
		// For some reason, WS_POPUP is the main issue on my system
		// NewStyle |= WS_POPUP;
		
		SetWindowLong(Wnd, GWL_STYLE, NewStyle);

		UpdateWindow(Wnd);
	}
}

// Ugly hack to let us keep the same code in the main file
#define SDL_SetWindowFullscreen(Window, Flags) \
	do {SDL_SetWindowFullscreen(Window, Flags); WIN32_SetWindowFullscreen(Window, Flags);} while(0)

#else

#define WM_Init(...)

#endif /* USE_WIN32_WINDOW_STYLE_HACK, _WIN32 */
