@echo off

setlocal

set src=metaballs-gl.c
set out=metaballs
REM set out=main

set win32_libs=opengl32.lib shell32.lib user32.lib
set libdir=lib/win32
set local_libs=%libdir%/SDL2.lib %libdir%/SDL2main.lib
set cflags=/O2

REM clang will complain about fopen
set defines=/D_CRT_SECURE_NO_WARNINGS

set ldflags=/incremental:no /subsystem:console

REM Compile
cl /nologo %src% /Iinclude %defines% %cflags% /Fe:%out% /link %ldflags% %win32_libs% %local_libs% && echo Compiled %out%.exe || exit
del *.obj

REM The copy command from cmd.exe doesn't accept forward slashes. Sad :(
copy lib\win32\SDL2.dll . > nul

endlocal


REM Sample compile command for mingw on Linux (tested on my system)
REM x86_64-w64-mingw32-gcc metaballs-gl.c -Iinclude -O2
REM x86_64-w64-mingw32-gcc metaballs-gl.o -o metaballs -lopengl32 -L lib/win32 -l:SDL2.lib -l:SDL2main.lib -l:SDL2.dll

@echo on
