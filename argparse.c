#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CONST_STR(s) {.data = s, .len = (sizeof(s) / sizeof(s)[0]) - 1}
#define OPTION(l, s) CONST_STR(l), CONST_STR(s)

#define AP_LAST_ARG {{NULL}, {NULL}}
/* Must be null terminated for C code */

// string_view is a null terminated array of bytes
// len does not take the null terminator into account 
typedef struct string_view
{
#ifdef __cplusplus
	const char *data = 0;
	size_t len = 0;
	// We don't make len const because frankly I don't understand C++'s copy and move constructor game
#else
	const char *data;
	size_t len;
#endif /* __cplusplus */

} string_view;


typedef struct opt
{
	string_view longname;
	string_view shortname;
	bool takesArg;
	string_view argname;
	string_view description;
}opt;




typedef enum failure_reason
{
	AP_ERR_NONE,
	AP_ERR_MISSING_ARG,
	AP_ERR_NO_COMMAND_LINE_ARGUMENTS,
	AP_ERR_UNKNOWN_ARG,
	AP_ERR_NULLPTR,
	AP_ERR_SHORTNAME_TOO_LONG,
	AP_ERR_HELP_REQUESTED,
} failure_reason;

typedef enum opt_type
{
	OPT_SHORT,
	OPT_LONG,
} opt_type;


typedef struct parser_state
{
	string_view CurrentArg;
	const char *CurrentOpt;
	string_view EmptyArg;
	const char *last_opt;
	int errorCode;
	int commandlineProcessed;
	int optionsProcessed;
	int initialized;
	const opt *Options;
	const char *progname;
	const char *usage;
} parser_state;

parser_state _ParserState = {.errorCode = AP_ERR_NONE};
opt _BuiltinHelpString = {OPTION("help", "h"), false, CONST_STR(""), CONST_STR("Show this help")};

void _BuiltinPrintOpt(const opt *Opt)
{
	if(!Opt) return;

	int spaces = 35;
	int written = 0;

	written += printf("    ");
	if(Opt->shortname.len > 0)
	{
		written += printf("-%s, ", Opt->shortname.data);
	}
	else
	{
		// Placeholder
		written += printf("    ");
	}
	if(Opt->longname.len > 0)
	{
		written += printf("--%s", Opt->longname.data);
	}

	if(Opt->takesArg)
	{
		written += printf("=%s", Opt->argname.data);
	}

	int spacesToPrint = spaces - written;
	for(int s = 0; s < spacesToPrint; s++)
	{
		putchar(' ');
	}
	printf("%s\n", Opt->description.data);
}

void _BuiltinHelp()
{
	if(_ParserState.usage)
	{
		printf("Usage: ");
		if(_ParserState.progname)
		{
			printf("%s ", _ParserState.progname);
		}
		printf("%s\n", _ParserState.usage);
	}

	int spaces = 25;
	for(int optionIndex = 0; _ParserState.Options[optionIndex].longname.data != NULL || _ParserState.Options[optionIndex].shortname.data != NULL; optionIndex++)
	{
		
		const opt *Opt = &_ParserState.Options[optionIndex];
		_BuiltinPrintOpt(Opt);
	}

	putchar('\n');
	_BuiltinPrintOpt(&_BuiltinHelpString);

}

#define FR(e) case (e): return #e
const char *ap_FailureReason()
{
	switch(_ParserState.errorCode)
	{
		FR(AP_ERR_NONE);
		FR(AP_ERR_NO_COMMAND_LINE_ARGUMENTS);
		FR(AP_ERR_MISSING_ARG);
		FR(AP_ERR_UNKNOWN_ARG);
		FR(AP_ERR_NULLPTR);
		FR(AP_ERR_SHORTNAME_TOO_LONG);
		FR(AP_ERR_HELP_REQUESTED);
		default:
			assert(0);
	}

	return NULL;
}
#undef FR

int ap_LastError()
{
	return _ParserState.errorCode;
}

bool StringsEqual(const char *a, const char *b, size_t len)
{
	if(!a || !b) return false;
	size_t i = 0;
	while(i < len)
	{
		if(a[i] != b[i]) break;
		i++;
	}
	return (i == len);
}

size_t StringLen(const char *s)
{
	size_t i = 0;

	if(!s) return i;

	while(s[i] != 0)
	{
		i++;
	}
	return i;
}

// Pass by ref here?
bool _OptionMatches(const opt *Opt, const char *arg, opt_type OptType)
{
	// Reject options that don't end in a null byte, or we can get a false match
	switch(OptType)
	{
		case OPT_LONG:
			return (StringsEqual(Opt->longname.data, arg, Opt->longname.len) && arg[Opt->longname.len] == 0);
		case OPT_SHORT:
			return (StringsEqual(Opt->shortname.data, arg, Opt->shortname.len) && arg[Opt->shortname.len] == 0);


	}
	return false;
}

int ap_ParseOpts(int argc, char * const *argv, const opt *Options)
{
	int result = -1;
	int i = 1 + _ParserState.commandlineProcessed;
	_ParserState.errorCode = AP_ERR_NONE;
	const opt *LastOpt = NULL;

	// Immediately fail if we match any of these conditions
	if(!Options || !argv)
	{
		_ParserState.errorCode = AP_ERR_NULLPTR;
		result = -1;
		goto end;
	}

	if((Options[_ParserState.optionsProcessed].longname.data == NULL 
	||  Options[_ParserState.optionsProcessed].shortname.data == NULL)
	&& (_ParserState.commandlineProcessed >= argc))
	{
		_ParserState.errorCode = AP_ERR_NONE;
		result = -1;
		goto end;
	}

	if(argc < 2)
	{
		_ParserState.errorCode = AP_ERR_NO_COMMAND_LINE_ARGUMENTS;
		result = -1;
		goto end;
	}

	if(!_ParserState.initialized)
	{
		_ParserState.initialized = 1;
		_ParserState.Options = Options;
	}

	for(; i < argc; i++)
	{
		const char *arg = argv[i];
		const char *option = NULL;

		_ParserState.CurrentOpt = arg;
		opt_type OptType = OPT_LONG;

		if(arg[0] == '-')
		{
			// Support unix-style 'stdin' option
			if(arg[1] == 0)
			{
				option = &arg[0];
			}
			else if(arg[1] == '-')
			{
				option = &arg[2];
				OptType = OPT_LONG;
			}
			// Todo: Make this support unicode. Don't rely on null terminated ascii strings
			// Todo: Support multiple unix-style single character shortnames in a single string argument
			// i.e. -asdf would specify a flag for shortnames 'a', 's', 'd', and 'f' respectively
			else if(arg[2] == 0) 
			{
				option = &arg[1];
				OptType = OPT_SHORT;
			}
			else
			{
				_ParserState.errorCode = AP_ERR_SHORTNAME_TOO_LONG;
				result = - 1;
				goto end;
			}
		}

		// @Hardcoded
		// Short circuit for help string?
		if(_OptionMatches(&_BuiltinHelpString, option, OptType))
		{
			_BuiltinHelp();
			_ParserState.CurrentOpt = arg;
			_ParserState.errorCode = AP_ERR_HELP_REQUESTED;
			result = -1;
			goto end;
		}

		for(int optionIndex = 0; Options[optionIndex].longname.data != NULL || Options[optionIndex].shortname.data != NULL; optionIndex++)
		{
			if(_OptionMatches(&Options[optionIndex], option, OptType))
			{
				_ParserState.commandlineProcessed++;
				_ParserState.optionsProcessed++;
				bool takesArg = Options[optionIndex].takesArg;
				if(takesArg)
				{
					_ParserState.last_opt = arg;
					i++;
					if(i < argc)
					{
						_ParserState.commandlineProcessed++;
						const char *argumentToOpt = argv[i];
						_ParserState.CurrentArg = (string_view){argumentToOpt, StringLen(argumentToOpt)};
						result = optionIndex;
						goto end;
					}
					else
					{
						_ParserState.errorCode = AP_ERR_MISSING_ARG;
						LastOpt = &_ParserState.Options[optionIndex];
						// _ParserState.CurrentArg;
						result = -1;
						goto end;
					}
				}
				else
				{
					// _ParserState.CurrentArg;
					result = optionIndex;
					goto end;
				}
			}
		}
		// We didn't match anything
		result = -1;
		_ParserState.errorCode = AP_ERR_UNKNOWN_ARG;
		goto end;
	}

end:
	return result;
}

string_view ap_OptArg()
{
	return _ParserState.CurrentArg;
}

const char *ap_OptOpt()
{
	return _ParserState.CurrentOpt;
}

void ap_SetProgramName(const char *s)
{
	_ParserState.progname = s;
}

void ap_SetUsageString(const char *s)
{
	_ParserState.usage = s;
}


#ifdef __cplusplus
}
#endif /* extern "C" */

#if 0
int main(int argc, char **argv)
{
	int i = 0;
	int iterations = 32;
	while((i = ParseOpts(argc, argv, Options)) != -1)
	{
		printf("%d\n", i);
		switch(i)
		{
			case A_ITERATIONS:
				printf("it\n");
				iterations = ParseU32(OptArg());
				break;
			case A_FULLSCREEN:
				printf("fs\n");
				break;
			case A_STDIN:
				printf("stdin\n");
				break;
			default:
				assert(0);
		}
	}

	printf("%s\n", FailureReason());

	return 0;
}
#endif
