#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <assert.h>
#include <stdlib.h>
#include <ctype.h>

/******************************************************
* Quake-style config file parser and utility library

* Accepts the following forms of key/value pairs:
====================================================
 name
 name value
 name "value"
 name "value1 value2 value3"
====================================================
* A key with an empty value is valid


*** Special note on the fourth form:

* The fourth form can have as many values as required
* Subsequent values `must` have at most 1 space separating them to be valid

* For instance, this string:
 name "value_x   value_y"
* Is equivlent to simply writing:
 name "value_x"

******************************************************/


typedef struct
{
	unsigned char *data;
	size_t len;
} cfg_string, cfg_buffer;

typedef struct
{
	cfg_string key;
	cfg_string value;
} cfg_key;

typedef struct
{
	const char *filename;
	cfg_buffer buffer;

	cfg_key *keys;
	size_t key_count;
	size_t key_cap;

	int should_free_buffer;

} cfg_context;

typedef struct
{
	const unsigned char *buffer;
	size_t len;
	size_t pos;
} cfg_stream;



/**********************************
* Public interface
**********************************/
int cfg_open(cfg_context *cfg, const char *filename);
/* 
   Opens a config file with fopen. 
   Call cfg_close to free all memory allocated during opening.
   Returns 1 on success, 0 otherwise.
*/


int cfg_open_mem(cfg_context *cfg, const unsigned char *buffer, size_t len);
/* 
   Opens a config file with the given memory buffer.
   Call cfg_close to free all memory allocated during opening.
   Returns 1 on success, 0 otherwise.
*/


void cfg_close(cfg_context *cfg);
/* 
   Frees all memory allocated during opening.
*/



int cfg_key_find(cfg_context *cfg, const char *name, size_t len, cfg_key **out_key);
/* 
   Tries to find a key with the given name. 
   Returns 1 on success, 0 otherwise. 
*/


int cfg_key_find_u(cfg_context *cfg, const char *name, cfg_key **out_key);
/*
   See cfg_key_fnd.
   This is a convenience method that uses strlen to calculate the length of the strings.
*/


cfg_key cfg_key_make(const unsigned char *keyname, size_t keylen, const unsigned char *value, size_t valuelen);
/* 
   Initializer for a cfg_key. 
   Does not allocate memory or free the memory passed to it. 
*/


cfg_key cfg_key_make_u(const unsigned char *keyname, const unsigned char *value);
/* 
   See cfg_key_make.
   This is a convenience method that uses strlen to calculate the length of the strings.
*/


cfg_string cfg_string_make(const unsigned char *data, size_t len);
/* 
   Initializer for cfg_string.
   Does not allocate memory or free the memory passed to it.
*/

int cfg_parse_int(cfg_string str, int *success);
/* 
   Parses an integer.
   Returns the converted integer if possible.

   A valid string takes the following form:
   [any amount of whitespace] <number> [any amount of whitespace]
  
   If the string matches this form and a conversion was performed, success is 1, 0 otherwise.
*/


int cfg_key_parse_int(cfg_key kv, int *success);
/* 
   Convenience wrapper. See cfg_parse_int.
*/


int cfg_key_strcmp(const char *valuename, cfg_key kv);
/* 
   Convenience function. 
   Returns 0 when kv.value is equal to valuename, non-zero otherwise.
*/


int cfg_set_int_from_key(cfg_context *cfg, cfg_string keyname, int *value_to_set);
/* 
   If a key by the name 'keyname' exists, and it holds an integer value, 'value_to_set' is changed to that value.
   Otherwise, 'value_to_set' remains unchanged.

   Returns 1 if the value was changed, 0 otherwise.
*/


int cfg_set_int_from_key_u(cfg_context *cfg, const char *keyname, int *value_to_set);
/* 
   See cfg_set_int_from_key
   This is a convenience method that uses strlen to calculate the length of the strings.
*/




cfg_key cfg_key_make(const unsigned char *keyname, size_t keylen, const unsigned char *value, size_t valuelen)
{
	cfg_key kv;
	kv.key = cfg_string_make(keyname, keylen);
	kv.value = cfg_string_make(value, valuelen);

	return kv;
}

cfg_key cfg_key_make_u(const unsigned char *keyname, const unsigned char *value)
{
	return cfg_key_make(keyname, strlen((char *)keyname), value, strlen((char *)value));
}


/**********************************
* Internal interface
**********************************/


/**********************************
* String
**********************************/
cfg_string cfg_string_make(const unsigned char *data, size_t len)
{
	cfg_string str;
	str.data = (unsigned char *)data;
	str.len = len;

	return str;
}

cfg_string cfg_string_make_u(const char *data)
{
	return cfg_string_make((unsigned char *)data, strlen(data));
}

void cfgi_string_free(cfg_string str)
{
	free(str.data);
}

// Null terminated for C compatability
cfg_string cfgi_string_dup(cfg_string str)
{
	cfg_string result = {0};
	if(str.len && str.data)
	{
		result.data = (unsigned char *)malloc(str.len + 1);
		result.len = str.len;
		memcpy(result.data, str.data, result.len);
		result.data[result.len] = 0;
	}

	return result;
}

/**********************************
* Stream
**********************************/
void cfgi_stream_init(cfg_stream *s, const unsigned char *buffer, size_t len)
{
	s->buffer = buffer;
	s->len = len;
	s->pos = 0;
}

int cfgi_stream_getline(cfg_stream *s, cfg_string *line)
{
	int result = 0;
	size_t start = s->pos;
	size_t idx = s->pos;
	while(idx < s->len && s->buffer[idx] != '\n')
	{
		idx++;
	}

	size_t end = idx;

	// Don't include the Windows CR code in the output
	if(idx > 1 && s->buffer[idx - 1] == '\r')
	{
		idx--;
	}
	
	size_t len = idx - start;

	// Skip the new line
	s->pos = end + 1;

	line->data = (unsigned char *)&s->buffer[start];
	line->len = len;

	if(s->pos <= s->len) result = 1;
	else result = 0;

	return result;
}

void cfgi_cfg_init(cfg_context *cfg)
{
	cfg_context c = {0};
	*cfg = c;
}

void str_print(cfg_string s)
{
	fwrite(s.data, 1, s.len, stdout);
	putchar('\n');
}


/**********************************
* Key
**********************************/
cfg_key cfgi_key_dup(cfg_key kv)
{
	cfg_key result;

	result.key = cfgi_string_dup(kv.key);
	result.value = cfgi_string_dup(kv.value);

	return result;
}

int cfgi_key_find(cfg_context *cfg, cfg_string name, cfg_key **out_key)
{
	int result = 0;

	for(int i = 0; i < cfg->key_count; i++)
	{
		int eq = strncmp((const char *)cfg->keys[i].key.data, (const char *)name.data, name.len);
		if(eq == 0)
		{
			result = 1;
			if(out_key) *out_key = &cfg->keys[i];
			break;
		}
	}

	return result;
}

void cfg_key_free(cfg_key kv)
{
	cfgi_string_free(kv.key);
	cfgi_string_free(kv.value);
}

int cfg_key_add(cfg_context *cfg, cfg_key kv)
{
	int result = 0;
	if(cfg->key_cap == 0)
	{
		cfg->key_cap = 32;
		cfg->keys = (cfg_key *)malloc(cfg->key_cap * sizeof *cfg->keys);
	}
	else
	{
		if(cfg->key_count == cfg->key_cap)
		{
			size_t newcap = cfg->key_cap * 2;
			void *oldmem = cfg->keys;
			void *newmem = realloc(cfg->keys, newcap * sizeof *cfg->keys);
			if(newmem != 0 && newmem != oldmem)
			{
				cfg->key_cap = newcap;
			}
			else
			{
				// Todo: Logging
				// We couldn't add any more space to the key array
			}
		}
	}
	cfg->keys[cfg->key_count++] = cfgi_key_dup(kv);

	return result;
}

// void cfg_enumerate(


int cfg_key_add_or_replace(cfg_context *cfg, cfg_key kv)
{
	cfg_key *existing_key = 0;

	if(cfgi_key_find(cfg, kv.key, &existing_key))
	{
		// Replace
		cfg_key_free(*existing_key);
		*existing_key = cfgi_key_dup(kv);
	}
	else
	{
		// Add
		cfg_key_add(cfg, kv);
	}

	return 0;
}

/**********************************
* Cfg context
**********************************/
int cfgi_open_mem(cfg_context *cfg, const unsigned char *buffer, size_t len);

int cfg_open(cfg_context *cfg, const char *filename)
{
	int result = 0;
	FILE *f = 0;
	struct stat sb;
	size_t size;
	unsigned char *buffer;
	int bytes_read;

	if(!cfg || !filename)
	{
		goto end;
	}

	cfgi_cfg_init(cfg);

	f = fopen(filename, "rb");
	if(!f) goto end;

	if(fstat(fileno(f), &sb) != 0) goto end;

	if(sb.st_size <= 0) goto end;
	
	size = sb.st_size;
	buffer = (unsigned char *)malloc(size + 1);


	bytes_read = fread(buffer, 1, size, f);
	assert(bytes_read == size);

	buffer[size] = 0;

	cfg->buffer.data = buffer;
	cfg->buffer.len = size;
	cfg->should_free_buffer = 1;

 	result = cfgi_open_mem(cfg, buffer, size);
end:
	if(f) fclose(f);
	return result;
}


int cfg_open_mem(cfg_context *cfg, const unsigned char *buffer, size_t len)
{
	cfgi_cfg_init(cfg);
	return cfgi_open_mem(cfg, buffer, len);
}

int cfgi_identifier_char_is_valid(int c)
{
	return isalnum(c) || (c == '_');
}

// Assumes cfg has already been initalized
int cfgi_open_mem(cfg_context *cfg, const unsigned char *buffer, size_t len)
{
	int result = 0;
	if(!cfg || !buffer || ! len)
	{
		return result;
	}

	cfg_stream s;
	cfg_string line;
	cfgi_stream_init(&s, buffer, len);


	const char comment = ';';

	while(cfgi_stream_getline(&s, &line))
	{
		int idx = 0;
		
		while(idx < line.len && isspace(line.data[idx]))
		{
			idx++;
		}

		if(idx == line.len) continue;
		if(line.data[idx] == comment) continue;

		// Parse the key identifier
		int key_begin = idx;
		while(idx < line.len && cfgi_identifier_char_is_valid(line.data[idx]))
		{
			idx++;
		}
		int key_end = idx;
		int key_len = key_end - key_begin;

		if(key_len == 0) continue;

		cfg_string keyname;
		cfg_string value;
		int val_begin = 0;
		int val_end = 0;
		int val_len = 0;

		// Parse the value identifier
		while(idx < line.len && isspace(line.data[idx]))
		{
			idx++;
		}

		// if(idx == line.len) continue;

		// Parse a quoted string
		if(line.data[idx] == '"')
		{
			// If we're here, we matched the first quotation mark `"`
			// Now search backwards to match the second one
			int quote_begin = idx;
			int quote_end = line.len - 1;
			while(quote_end < quote_begin && line.data[quote_end] != '"')
			{
				quote_end--;
			}

			if(quote_end == quote_begin) continue;

			// If we're here, we matched the second quotation mark `"`
			// Find the length of the value string
			// The following forms are acceptable:
			// width "800"
			// dimensions "800 600"
			// player_start "100, 200 300     "
			// Specifically, a value substring can be seperated by exactly `one` space to allow groupings
			// Trailing spaces after the last substring are ignored

			val_begin = quote_begin + 1;
			int value_idx = val_begin;
			while(value_idx < quote_end)
			{
				while(value_idx < quote_end && !isspace(line.data[value_idx]))
				{
					value_idx++;
				}

				// Check if the next character is not a space
				int c = line.data[value_idx];
				if(c == ' ' || c == '\t')
				{
					int next = value_idx + 1;
					if(next < quote_end && !isspace(line.data[next]))
					{
						value_idx = next;
						continue;
					}
					else
					{
						break;
					}
				}
			}
			val_end = value_idx;
			val_len = val_end - val_begin;
		}
		// Parse an unquoted string
		// This is a continuous string of alphanumeric characters
		// Trailing spaces, and any other characters for that matter, are ignored
		// The following forms are acceptable:

		// width 800
		// height 600
		// score 50
		// texture_format RGBA_32
		
		else
		{
			val_begin = idx;
			while(idx < line.len && !isspace(line.data[idx]))
			{
				idx++;
			}
			val_end = idx;
			val_len = val_end - val_begin;
		}

		assert(val_len >= 0);

		keyname = cfg_string_make(&line.data[key_begin], key_len);
		value = cfg_string_make(&line.data[val_begin], val_len);
		
		cfg_key kv;
		kv.key = keyname;
		kv.value = value;

		// We now have a key value pair
		// Add it to the config file (or replace it)
		cfg_key_add_or_replace(cfg, kv);
	}

	result = 1;

end:
	return result;
}


int cfg_key_find(cfg_context *cfg, const char *name, size_t len, cfg_key **out_key)
{
	cfg_string str = cfg_string_make((unsigned char *)name, len);
	return cfgi_key_find(cfg, str, out_key);
}

int cfg_key_find_u(cfg_context *cfg, const char *name, cfg_key **out_key)
{
	return cfg_key_find(cfg, name, strlen(name), out_key);
}

int cfg_key_parse_int(cfg_key kv, int *success)
{
	return cfg_parse_int(kv.value, success);
}


/**********************************
* Parsing functions
**********************************/
int cfg_parse_int(cfg_string str, int *success)
{
	int n = 0;
	
	size_t i = 0;
	size_t len = str.len;
	
	int ok = 0;

	while(i < len && isspace(str.data[i]))
	{
		i++;
	}

	if(isdigit(str.data[i]))
	{
		ok = 1;

		while(i < len && isdigit(str.data[i]))
		{
			int c = str.data[i];
			n = (n * 10) + (c - '0');
			i++;
		}
	}

	// Check for any invalid characters that follow
	while(i < len)
	{
		if(!isspace(str.data[i]))
		{
			ok = 0;
			break;
		}
		i++;
	}

	if(success) *success = ok;

	return n;
}

int cfg_key_strcmp(const char *valuename, cfg_key kv)
{
	return strncmp(valuename, (const char *)kv.value.data, kv.value.len);
}

char *cfg_value_dup(cfg_key kv)
{
	cfg_string s = cfgi_string_dup(kv.value);
	return (char *)s.data;
}

int cfg_set_int_from_key(cfg_context *cfg, cfg_string keyname, int *value_to_set)
{
	cfg_key *key = 0;
	int changed = 0;
	if(cfgi_key_find(cfg, keyname, &key))
	{
		int success = 0;
		int value = cfg_key_parse_int(*key, &success);
		if(success)
		{
			changed = 1;
			*value_to_set = value;
		}
	}

	return changed;
}

int cfg_set_int_from_key_u(cfg_context *cfg, const char *keyname, int *value_to_set)
{
	cfg_string str = cfg_string_make_u(keyname);
	return cfg_set_int_from_key(cfg, str, value_to_set);
}


void cfg_close(cfg_context *cfg)
{
	if(!cfg) return;
	
	cfg_context c = {0};

	for(int i = 0; i < cfg->key_count; i++)
	{
		cfg_key_free(cfg->keys[i]);
	}
	free(cfg->keys);

	if(cfg->should_free_buffer)
	{
		cfgi_string_free(cfg->buffer);
	}

	*cfg = c;
}

#ifdef __cplusplus
}
#endif
