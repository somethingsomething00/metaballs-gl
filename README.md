# Metaballs GL

Software and GLSL implementations of a metaball renderer.

This program originally started out as a very basic metaball software renderer.

Over several days I expanded it to support shaders as well as an SSE2 software path, which although is no match for the shader version in terms of speed, was nevertheless pretty fun to come up with, and at least gives you the ability to display more objects without frame drops.

GPUs are pretty powerful beasts, that's for sure.

![thumbnail](./thumbnail.png)

## Building
* Depends on:
	* C compiler (Tested with `clang`, `gcc`, and `cl`)
	* SDL2 (DLL and import libraries included for Windows)
	* OpenGL2 (Uses `SDL_opengl.h` headers)
	* GLSL version `150`
	* `SSE2` instruction set capable cpu (optional, can be disabled in [config.h](./config.h))

### Linux
```console
./build.sh
./metaballs
```

### Windows
```console
build.bat
metaballs.exe
```

## Controls
```
Left Click..........Spawn metaball
Right Click.........Despawn metaball
Mouse Movement......Move metaball around screen

F1..................Help and full list of controls


* Note: This program accepts command line arguments
* Run ./metaballs --help to see a full list of options
```


## References and inspiration:
[The Coding Train Challenge #28: Metaballs](https://www.youtube.com/watch?v=ccYLb7cLB1I)

[Rendering This in Real-Time on CPU](https://www.youtube.com/watch?v=PUu5kjoWw0k)

[stb_easy_font](https://github.com/nothings/stb/blob/master/stb_easy_font.h)
