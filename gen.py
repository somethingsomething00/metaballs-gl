# OpenGL function pointer loader generator
# Requires a header with OpenGL function signatures as input
# Currently, this is hardcoded to "gl_extensions.h"
# This parser is far from robust, but it suffices when given a simple header file

# Outputs to stdout. You'll need to pipe the output to a file for use in your C program

# Once generated, include the file in your C program.
# Call GL_LoadExtensions with your function loader of choice
# It must conform to the following signature
# void *GetGLProcAddress(const char *ProcName);

# Some examples:

# SDL_GL_GetProcAddress
# glfwGetProcAddress
# glXGetProcAddress
# wglGetProcAddress

from dataclasses import dataclass

from sys import argv

@dataclass
class function_id:
    typ: str
    name: str
    signature: str
    pfn: str




def gather_functions(filename: str):
    header = open(filename)
    lines = header.read().split('\n')

    functions = []
    for line in lines:
        line = line.lstrip()
        if line.startswith("//"):
            continue

        decl = line.split("GLAPI")
        if len(decl) > 1:
            parts = decl[1].split(' ')

            typ = parts[1]
            name = parts[3]
            signature = ""
            
            # It might be faster just to split based on the function name again, we'll only know at scale
            sigsplit = decl[1].split(name)
            
            # Now get rid of the semicolon
            signature = sigsplit[1].split(';')[0]

            pfn = "pfn_" + name
            
            # Concatenate the rest instead of using split
            # for i in range(4, len(parts)):
                # signature += parts[i]
                # signature += ' '

            # Now we have all of the parts
            # Let's add it to the list
            func = function_id(typ, name, signature, pfn)
            functions.append(func)
            # print(signature)

    return functions


def generate_typedefs(functions: [function_id]):
    for f in functions:
        prefix = "typedef "
        pfn = "(APIENTRY *" + f.pfn + ")"

        print(prefix, f.typ, " ", pfn, f.signature, ";", sep='')

def generate_function_pointer_decls(functions: [function_id]):
    for f in functions:
        decl = f.name + ";"
        print(f.pfn, decl)


# How to do formatted output in Python:
# https://stackoverflow.com/a/5310077
# Original question:
# https://stackoverflow.com/questions/5309978/sprintf-like-functionality-in-python
def sprintf(fmt, *args):
    s = fmt % args
    return s

def fmt_printf(fmt, *args):
    s = fmt % args
    print(s, sep = '')

def c_heading(text: str):
    print("")
    print("/************************")
    print("* " + text)
    print("************************/")

def c_comment(text: str):
    print("//", text)

# Where all of the function pointers are actually loaded
def generate_init_function(function_loader: function_id, functions: [function_id]):

    sig = sprintf("(%s %s)", function_loader.pfn, function_loader.name)
    loader  = function_id("int", "GL_LoadExtensions", sig, "ignore")
    print(loader.typ, loader.name, loader.signature)

    print("{")

    spaces = "    "


    # Define a loader macro
    macro_name = "LOAD_GL_FUNCTION"
    err_string = "fprintf(stderr, \"ERROR: Could not load OpenGL function `%s`\", #n);"
    varname = "success"
    
    macro_nl = "\\\n"
    macro_list = []
    macro_list.append(sprintf("#define %s(n) do {\\\n", macro_name))
    macro_list.append(sprintf("    n = (pfn_##n)%s(#n);", function_loader.name))
    macro_list.append(macro_nl)

    macro_list.append("    if(!n) {")
    macro_list.append(macro_nl)

    macro_list.append(sprintf("      %s", err_string))
    macro_list.append(macro_nl)

    macro_list.append(sprintf("      %s = 0;", varname))
    macro_list.append(macro_nl)

    macro_list.append("    }")
    macro_list.append(macro_nl)

    macro_list.append("}while(0)\n\n")

    for l in macro_list:
        print(l, end='')


    # Result var
    fmt_printf("    int %s = 1;\n", varname)
    

    # Now load the functions
    for f in functions:
        fmt_printf("    %s(%s);", macro_name, f.name)

        # print(spaces, f.name, "=", loader_call)
    fmt_printf("\n#undef %s", macro_name)

    fmt_printf("\n%sreturn %s;", spaces, varname);

    print("}")


def generate_function_loader_prototype(name: str):
    pfn = "pfn_" + name
    typ = "void *"
    sig = "(const char *)"

    func = function_id(typ, name, sig, pfn)

    
    typedef = sprintf("typedef %s(*%s)%s;", func.typ, func.pfn, func.signature)
    print(typedef)

    return func


def generate_includes():
    print("#include <stdio.h>")

def cpp_header_guard_begin():
    fmt_printf("#ifdef __cplusplus")
    fmt_printf("extern \"C\" {")
    fmt_printf("#endif")

def cpp_header_guard_end():
    fmt_printf("#ifdef __cplusplus")
    fmt_printf("}")
    fmt_printf("#endif")


def main():
    functions = gather_functions("gl_extensions.h")
   
    c_comment("Generated with " + argv[0] + "\n")
  
    cpp_header_guard_begin()

    c_heading("Includes")
    generate_includes()
    
    c_heading("Typedefs")
    generate_typedefs(functions)
    
    c_heading("Decls")
    generate_function_pointer_decls(functions)
  
    c_heading("Loader")
    prototype = generate_function_loader_prototype("gl_get_proc_address")
    generate_init_function(prototype, functions)


    print("")
    cpp_header_guard_end()

main()
