#!/bin/sh

# OpenGL build
cc="clang"
src="metaballs-gl.c"
bin="metaballs"
lib="-lm -lGL -lSDL2"
inc="-Iinclude"
cflags="-O2 -march=native -msse2 -ffast-math -Wvla"

compile_command="$cc $src -o $bin $inc $lib $cflags"
echo $compile_command
$compile_command && echo "Compiled $bin" || exit


