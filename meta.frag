// Version 150 is the earliest version I've found that supports origin_upper_left
// Alternatively, I can pass in the inverted coordinates as a uniform and compute them here
#version 150

// Top left origin just like the software version
layout(origin_upper_left) in vec4 gl_FragCoord;


/**********************************
* Regular uniforms
***********************************/
uniform vec2 inv_window_dimensions;
uniform vec2 inv_viewport_scale;



/**********************************
* Metaball config
***********************************/
// This can't be too high due to the hard limit of uniform variables
// On 4.x OpenGL versions, we can use uniform buffers or shader storage buffers
#define MAX_METABALL_COUNT 128

/***************************************************************
* This is where uniform buffer objects really come in handy
* I'm just laying this out as a flat buffer of floats instead
****************************************************************/
// float[2] pos
// float[2] radius (alignment reasons)
#define METABALL_PROP_COUNT 4

uniform float metaballs[MAX_METABALL_COUNT * METABALL_PROP_COUNT];
uniform int MetaballCount = 2;


#define rsqrt inversesqrt
float calc_metaball(vec2 ballpos, vec2 fragpos, float radius)
{
	vec2 delta = fragpos - ballpos;
	float l2 = dot(delta, delta);
	float len = rsqrt(l2);
	float d = radius * len;
	float s = d * d;

	return s;
}

void main()
{
	// Note: gl_FragCoord is in regular window coordinates
	vec2 pixel_pos = gl_FragCoord.xy * inv_viewport_scale;
	float sum = 0;
	int stride = 0;
	for(int i = 0; i < MetaballCount; i++)
	{
		vec2 pos = vec2(metaballs[stride + 0], metaballs[stride + 1]);
		float radius = metaballs[stride + 2];
		sum += calc_metaball(pos, pixel_pos, radius);
		
		stride += METABALL_PROP_COUNT;
	}

	float s = sum;


	vec3 weights = vec3(0.8, 0.5, 0.4);
	vec3 c = (s * weights);
	vec4 output_color = vec4(c, 1); 

	// Remove this line for a constant colour
	output_color.xy *= (pixel_pos * inv_window_dimensions);

	float total = output_color.x + output_color.y + output_color.z;
	
	// The smaller this number, the larger the blobs appear to be
	// if(total <= 0.5) discard;

	gl_FragColor = output_color;
	// gl_FragColor = vec4(MousePos / vec2(800, 600), 0, 1);

}
