// #include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>


#include <stdio.h>
#include <stdint.h>
#include <assert.h>


#include "gl_extension_loader.c"
#include "profiler3.c"
#include "random.c"
#include "cpu.c"
#include "argparse.c"
#include "cfg.c"


#define PREFERRED_INTERNAL_PIXEL_FORMAT (GL_RGBA)
#define PREFERRED_CLIENT_PIXEL_FORMAT (GL_BGRA_EXT)

#include "shader_loader.h"
#include "shader_loader.c"

#include "console.c"
#include "msg.c"

#include "config.h"

/**********************************
* Macros
**********************************/
#define PACK_BGRA(r, g, b, a) ((b) << 0 | (g) << 8 | (r) << 16 |  a << 24)
#define UNPACK_BGRA(color, comp) ( ((color) >> ((comp) * 8)) & 0xFF)

#define Min(a, b) ((a) < (b)) ? (a) : (b)
#define Max(a, b) ((a) > (b)) ? (a) : (b)
#define Clamp(have, mi, ma) Min(Max((have), (mi)), (ma))


#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

/**********************************
* Types
**********************************/
typedef uint32_t u32;
typedef uint64_t u64;

/**********************************
* Forward declarations
**********************************/
void R_PrepareRenderMode();


typedef struct
{
	int GlFinishEnabled;
	GLuint TextureFilter;
} config;


typedef struct
{
	int width;
	int height;
	u32 *pixels;
	
	GLuint id;
	GLuint internalformat;
	GLuint clientformat;
	
} texture;


typedef struct
{
	// Position
	float x;
	float y;
	
	// Radius
	float r;
	float original_r;
	
	// Scaled dir
	float dx;
	float dy;
	
	// Normalised dir for some calculations
	// Namely the orthogonal direction to the current one
	float dir_x;
	float dir_y;
	
	float phase_offset;
	float jitter_offset;
	float osc_offset;
	
} metaball;


typedef struct
{
	float CpuUsage;
	const char *RendererVersion;
	const char *GPU;
	
	float PerformanceFrequency;
	
	float FrameTime;
	float RenderTime;
	
	float StartTime;
	float RunningTime;
	
	int Paused;
	
	int Fullscreen;
} system_info;


// Common "uniform" variables between the software and hardware renderer
typedef struct
{
	int MetaballCount;
	float inv_WindowHeight;
	float inv_WindowWidth;
} uniforms;


/**********************************
* Globals
**********************************/
texture Screen = {0};
int Running = 1;

#define MAX_METABALL_COUNT  128
metaball metaballs[MAX_METABALL_COUNT];

system_info SystemInfo = {0};
uniforms Uniforms = {0};

int ShowHelp = 0;
int ShowSystemInfo = 0;


config cfg = {0};

void CFG_Init()
{
	config c = 
	{
		.GlFinishEnabled = 0,
		.TextureFilter = GL_LINEAR,
	};

	cfg = c;

	cfg_context ctx;
	if(!cfg_open(&ctx, "config.cfg")) return;
	cfg_set_int_from_key_u(&ctx, "gl_finish", &cfg.GlFinishEnabled);

	int nearest = 0;
	cfg_set_int_from_key_u(&ctx, "gl_nearest", &nearest);
	if(nearest) cfg.TextureFilter = GL_NEAREST;

	cfg_close(&ctx);
}

metaball MetaballNew(float x, float y, float radius)
{
	metaball m = {0};
	m.x = x;
	m.y = y;
	m.r = radius;
	m.original_r = radius;
	
	return m;
}

float radians(float degrees)
{
	return degrees * ((float)(3.141592654 / 180.0));
}

/**************************************************************
* Configuration for the random initilization of the metaballs
**************************************************************/
// #define RADIUS_MIN 10
// #define RADIUS_MAX 60

#define RADIUS_MIN 10
#define RADIUS_MAX 60

// #define RADIUS_MIN 10
// #define RADIUS_MAX 15

// #define RADIUS_MIN 80
// #define RADIUS_MAX 140

#define SPEED_MIN 50
#define SPEED_MAX 75

#define PHASE_MIN 0.5
#define PHASE_MAX 10

#define JITTER_OFFSET_MIN 0.1
#define JITTER_OFFSET_MAX 0.2

#define OSC_OFFSET_MIN -0.1
#define OSC_OFFSET_MAX 0.1


// #define RADIUS_MIN 5
// #define RADIUS_MAX 20

// #define SPEED_MIN 50
// #define SPEED_MAX 300

metaball MetaballNewRandomized()
{
	float radius = random_f32_range(RADIUS_MIN, RADIUS_MAX);
	float dir = radians(random_f32_range(0, 360));
	
	float x = random_f32_range(radius, WINDOW_WIDTH - radius);
	float y = random_f32_range(radius, WINDOW_HEIGHT - radius);
	
	float speed = random_f32_range(SPEED_MIN, SPEED_MAX);
	
	float dir_x = cos(dir);
	float dir_y = sin(dir);
	float dx = dir_x * speed;
	float dy = dir_y * speed;
	float phase_offset = random_f32_range(PHASE_MIN, PHASE_MAX);
	float jitter_offset = random_f32_range(JITTER_OFFSET_MIN, JITTER_OFFSET_MAX);
	float osc_offset = random_f32_range(OSC_OFFSET_MIN, OSC_OFFSET_MAX);
	
	
	metaball m;
	m.x = x;
	m.y = y;
	m.r = radius;
	m.original_r = radius;
	m.dx = dx;
	m.dy = dy;
	m.dir_x = dir_x;
	m.dir_y = dir_y;
	m.osc_offset = osc_offset;
	
	return m;
}

void MetaballUpdate(metaball *Metaball, float dt, float timestamp_ms)
{
	// Attempt at implementing a "jitter" pattern
	// It somewhat works, the parameters are very very specific
	// float scale = 0.5;
	// float freq = 0.025;
#if 0
	float scale = 0.5;
	float freq = 0.025 + Metaball->jitter_offset;
	
	float ts = timestamp_ms + Metaball->phase_offset;
	float sx = sin(ts * freq) * scale;
	float sy = sin(ts * freq) * scale;
	
	// Orthogonal vector to the current direction
	// Todo: Cache this and update it only when the direction changes
	float ortho_x = Metaball->dir_y * sx;
	float ortho_y = -Metaball->dir_x * sy; // Negated y
	
	
	float next_x = Metaball->x + (Metaball->dx * dt) + (ortho_x);
	float next_y = Metaball->y + (Metaball->dy * dt) + (ortho_y);
	float radius = Metaball->r;
#else
	// Have some fun with the radius
	// Shrinks/expands the radius based on frequency and range
	// Remap to 0.75 to 1.25
	// const float halfrange = (1.15 - 0.85) * 0.5;
	// const float rad_freq = 4 + Metaball->osc_offset;
	// float r = sin(SystemInfo.RunningTime * rad_freq) * halfrange + 1;
	// Metaball->r = Metaball->original_r * r;
	
	// No jitter, just advance by dx and dy
	float next_x = Metaball->x + (Metaball->dx * dt);
	float next_y = Metaball->y + (Metaball->dy * dt);
	float radius = Metaball->r;
#endif
	
	int collided = 0;
	
	
	if(next_x + radius  > WINDOW_WIDTH)
	{
		Metaball->dx *= -1;
		Metaball->dir_x *= -1;
		next_x = WINDOW_WIDTH - radius;
		collided = 1;
	}
	else if(next_x - radius < 0)
	{
		Metaball->dx *= -1;
		Metaball->dir_x *= -1;
		next_x = 0 + radius;
		collided = 1;
	}
	
	if(next_y + radius > WINDOW_HEIGHT)
	{
		Metaball->dy *= -1;
		Metaball->dir_y *= -1;
		next_y = WINDOW_HEIGHT - radius;
		collided = 1;
	}
	else if(next_y - radius < 0)
	{
		Metaball->dy *= -1;
		Metaball->dir_y *= -1;
		next_y = 0 + radius;
		collided = 1;
	}
	
	// Change jitter direction
	if(collided)
	{
	}
	
	Metaball->x = next_x;
	Metaball->y = next_y;
}

u32 LerpColorComponent(u32 a, u32 b, float t)
{
	float fa = a;
	float fb = b;
	return fa * t + ((1 - t) * fb);
}

u32 LerpColorBGRA(u32 c0, u32 c1, float t)
{
	u32 b0 = UNPACK_BGRA(c0, 0);
	u32 g0 = UNPACK_BGRA(c0, 1);
	u32 r0 = UNPACK_BGRA(c0, 2);
	u32 a0 = UNPACK_BGRA(c0, 3);
	
	u32 b1 = UNPACK_BGRA(c1, 0);
	u32 g1 = UNPACK_BGRA(c1, 1);
	u32 r1 = UNPACK_BGRA(c1, 2);
	u32 a1 = UNPACK_BGRA(c1, 3);
	
	u32 b = LerpColorComponent(b0, b1, t);
	u32 g = LerpColorComponent(g0, g1, t);
	u32 r = LerpColorComponent(r0, r1, t);
	u32 a = LerpColorComponent(a0, a1, t);
	
	return PACK_BGRA(r, g, b, a);
}

#if 0
// Inline assembly version
// ~2ms faster per frame than doing a function call per pixel from the nasm version
// The resolution tested was 1600x1000 pixels
float rsqrt(float x)
{
	// The Yz constraint means use xmm0
	// + means that the register is both read from and written to
	asm
	(
	 ".intel_syntax noprefix\n"
	 "rsqrtss xmm0, xmm0\n"
	 ".att_syntax"
	 : "+Yz"(x)  /* Output */
	 :
	 );
	return x;
}
#else
// Clang and GCC seem to recognize this pattern
// Pretty cool
float rsqrt(float x)
{
	return 1.0 / sqrtf(x);
}
#endif

void ExitDemoWithCode(int code)
{
	Running = 0;
	exit(code);
}

void ExitDemo()
{
	ExitDemoWithCode(0);
}

typedef enum
{
	R_HW,
	R_SW,
	R_SW_SLOW,
} render_mode;

typedef struct
{
	int LocMetaballStructBase;
	int LocWindowDimensions;
	int LocMetaballCount;
	int LocViewportScale;
	
	int Shader;
	
	render_mode RenderMode;
	
	int ShouldClearSoftwareBuffer;
	
	float ViewportWidth;
	float ViewportHeight;
	
} renderer;

renderer r = {0};

void R_Init(render_mode RenderMode)
{
	r.Shader = ShaderProgramCreate("meta.vert", "meta.frag");
	glUseProgram(r.Shader);
	
	// Hack: Get the location of the first metaball array member in meta.frag and upload data to it
	// This is where uniform buffers would be nice
	r.LocMetaballStructBase = glGetUniformLocation(r.Shader, "metaballs[0]");
	r.LocWindowDimensions = glGetUniformLocation(r.Shader, "inv_window_dimensions");
	r.LocMetaballCount = glGetUniformLocation(r.Shader, "MetaballCount");
	r.LocViewportScale = glGetUniformLocation(r.Shader, "inv_viewport_scale");
	
	glUseProgram(0);
	
	
	r.RenderMode = RenderMode;
	
	glClearColor(0, 0, 0, 0);
	
	R_PrepareRenderMode();
}

void R_Viewport()
{
	glViewport(0, 0, r.ViewportWidth, r.ViewportHeight);
}

void R_UpdateMetaballUniforms()
{
	struct glsl_metaball
	{
		float pos[2];
		float radius[2];
	};
	
	struct glsl_metaball mb[MAX_METABALL_COUNT];
	
	for(int i = 0; i < Uniforms.MetaballCount; i++)
	{
		mb[i].pos[0] = metaballs[i].x;
		mb[i].pos[1] = metaballs[i].y;
		mb[i].radius[0] = metaballs[i].r;
	}
	
	glUniform1fv(r.LocMetaballStructBase, 4 * Uniforms.MetaballCount, (float *)&mb[0]);
	
	glUniform2f(r.LocWindowDimensions, Uniforms.inv_WindowWidth, Uniforms.inv_WindowHeight);
	glUniform1i(r.LocMetaballCount, Uniforms.MetaballCount);
	
	float inv_viewport_w = 1.0 / (r.ViewportWidth / (float)WINDOW_WIDTH);
	float inv_viewport_h = 1.0 / (r.ViewportHeight / (float)WINDOW_HEIGHT);
	glUniform2f(r.LocViewportScale, inv_viewport_w, inv_viewport_h);
}

texture TextureCreate(int width, int height, GLuint Filter)
{
	texture Texture = {0};
	
	GLuint id;
	glGenTextures(1, &id);
	
	Texture.width = width;
	Texture.height = height;
	Texture.id = id;
	Texture.internalformat = PREFERRED_INTERNAL_PIXEL_FORMAT;
	Texture.clientformat = PREFERRED_CLIENT_PIXEL_FORMAT;
	
	Texture.pixels = calloc(width * height, sizeof *Texture.pixels);
	assert(Texture.pixels);
	
	glBindTexture(GL_TEXTURE_2D, id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, Filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, Filter);
	
	glTexImage2D(GL_TEXTURE_2D, 0, Texture.internalformat, Texture.width, Texture.height, 0, Texture.clientformat, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	return Texture;
}

void TextureUpdate(texture *Texture)
{
	glTexImage2D(GL_TEXTURE_2D, 0, Texture->internalformat, Texture->width, Texture->height, 0, Texture->clientformat, GL_UNSIGNED_BYTE, Texture->pixels);
}

// Assumes that the projection matrix has been set
// glOrtho(0, 1, 1, 0, 0, 1)
void TextureRender(texture *Texture)
{
	glColor3f(1, 1, 1);
	glBegin(GL_QUADS);
	
	glTexCoord2f(0, 0);
	glVertex2f(0, 0);
	
	glTexCoord2f(0, 1);
	glVertex2f(0, 1);
	
	glTexCoord2f(1, 1);
	glVertex2f(1, 1);
	
	glTexCoord2f(1, 0);
	glVertex2f(1, 0);
	
	glEnd();
}

void TextureUpdateAndRender(texture *Texture)
{
	TextureUpdate(Texture);
	TextureRender(Texture);
}

void RenderWithHardware()
{
	R_UpdateMetaballUniforms();
	
	glClear(GL_COLOR_BUFFER_BIT);
	
	glBegin(GL_QUADS);
	
	glVertex2f(-1, -1);
	glVertex2f(-1, 1);
	glVertex2f(1, 1);
	glVertex2f(1, -1);
	
	glEnd();
}

float calc_metaball(float ball_x, float ball_y, float frag_x, float frag_y, float ball_radius)
{
	float dx = frag_x - ball_x;
	float dy = frag_y - ball_y;
	float l2 = dx * dx + dy * dy;
	
	float len = rsqrt(l2);
	
	float d = ball_radius * len;
	float s = d * d;
	
	return s;
}

// Optimized by precalculating dy outside of the x loop
float calc_metaball_soft(float ball_x, float frag_x, float dy2, float ball_radius)
{
	float dx = frag_x - ball_x;
	float l2 = dx * dx + dy2;
	
	float len = rsqrt(l2);
	
	float d = ball_radius * len;
	float s = d * d;
	
	return s;
}

// If we don't have anything to render, don't waste cycles in the pixel loop, and bail early
// Yeah we could rely on glClear but that's no fun :)
int SoftEarlyReturn()
{
	if(Uniforms.MetaballCount < 1)
	{
		if(r.ShouldClearSoftwareBuffer)
		{
			r.ShouldClearSoftwareBuffer = 0;
			memset(Screen.pixels, 0, Screen.width * Screen.height * sizeof *Screen.pixels);
			TextureUpdate(&Screen);
		}
		TextureRender(&Screen);
		return 1;
	}
	return 0;
}

// SSE2 version of the RenderWithSoftware
#if !NO_SSE2_RENDERER
#include <immintrin.h>

u32 pack_pixel_bgra(u32 r, u32 g, u32 b, u32 a)
{
	return PACK_BGRA(r, g, b, a);
}

void RenderWithSoftwareSSE2()
{
	if(SoftEarlyReturn())
	{
		return;
	}
	
	struct ball_sse
	{
		float x;
		float y;
		float r;
		float pad;
		__m128 dy2_precalc;
	};
	
	// Minimize cache misses for metaball access
	struct ball_sse data_sse[MAX_METABALL_COUNT];
	for(int i = 0; i < Uniforms.MetaballCount; i++)
	{
		data_sse[i].x = metaballs[i].x;
		data_sse[i].y = metaballs[i].y;
		data_sse[i].r = metaballs[i].r;
	}
	
	
#define _mm_square(a) _mm_mul_ps((a), (a))
#define _mm_clamp(a, lo, hi) _mm_max_ps(_mm_min_ps((a), (hi)), (lo))
	
	const int width = Screen.width;
	const int height = Screen.height;
	
	float inv_window_width = Uniforms.inv_WindowWidth;
	
	assert((width % 4) == 0);
	
	__m128i *pixels128 = (__m128i *)Screen.pixels;
	int p = 0;
	for(int y = 0; y < height; y++)
	{
		
		// Modulate red and green by the pixel position
		// wy modulates the green channel
		float wy = Uniforms.inv_WindowHeight * y;
		
		// Precalculate dy squared
		for(int b = 0; b < Uniforms.MetaballCount; b++)
		{
			float by = data_sse[b].y;
			__m128 dy = _mm_set1_ps(y - by);
			data_sse[b].dy2_precalc = _mm_square(dy);
		}
		
		// Note: Don't set each element of frag_x, just keep an accumulator!
		__m128i xs = _mm_setr_epi32(0, 1, 2, 3);
		for(int x = 0; x < width; x += 4)
		{
			__m128 frag_x = _mm_cvtepi32_ps(xs);
			__m128 sum = _mm_setzero_ps();
			for(int b = 0; b < Uniforms.MetaballCount; b++)
			{
				// This is equivalent to calc_metaball, except with dy squared precalculated
				
				float _ball_x = data_sse[b].x;
				float _ball_radius = data_sse[b].r;
				
				__m128 ball_x = _mm_set1_ps(_ball_x);
				__m128 dx = _mm_sub_ps(frag_x, ball_x);
				
				__m128 l2 = _mm_add_ps(_mm_square(dx), data_sse[b].dy2_precalc);
				__m128 len = _mm_rsqrt_ps(l2);
				
				__m128 ball_radius = _mm_set1_ps(_ball_radius);
				__m128 d = _mm_mul_ps(len, ball_radius);
				__m128 s = _mm_mul_ps(d, d);
				
				sum = _mm_add_ps(sum, s);
			}
			
			// Add xs accumulator
			__m128i four = _mm_set1_epi32(4);
			xs = _mm_add_epi32(xs, four);
			
			typedef union
			{
				__m128 sse;
				float f[4];
			} sse_reg;
			
			// Each component of the sum register is PER PIXEL
			// Each element of pixelsums operates on 1 pixel, for a total of 4
			__m128 pixelsums[4];
			// sse_reg sumz;
			// sumz.sse = sum;
			
			// pixelsums[0] = _mm_set1_ps(sumz.f[0]);
			// pixelsums[1] = _mm_set1_ps(sumz.f[1]);
			// pixelsums[2] = _mm_set1_ps(sumz.f[2]);
			// pixelsums[3] = _mm_set1_ps(sumz.f[3]);
			
			// I think this is equivalent to using the union?
			// Not sure about speed differences
			pixelsums[0] = _mm_shuffle_ps(sum, sum, _MM_SHUFFLE(0, 0, 0, 0));
			pixelsums[1] = _mm_shuffle_ps(sum, sum, _MM_SHUFFLE(1, 1, 1, 1));
			pixelsums[2] = _mm_shuffle_ps(sum, sum, _MM_SHUFFLE(2, 2, 2, 2));
			pixelsums[3] = _mm_shuffle_ps(sum, sum, _MM_SHUFFLE(3, 3, 3, 3));
			
			
			
			// Calculate inv window factor per x pixel
			// Modulate red and green by the pixel position
			// wx modulates the red channel
			__m128 wx = _mm_mul_ps(_mm_set1_ps(inv_window_width), frag_x);
			__m128 inv_window[4];
			sse_reg wx4;
			wx4.sse = wx;
			
			
			__m128 ff = _mm_set1_ps(255);
			__m128 zero = _mm_set1_ps(0);
			__m128 weights = _mm_setr_ps(0.8, 0.5, 0.4, 1);
			__m128 factor = _mm_mul_ps(weights, ff);
			
			
			sse_reg pixelsf[4];
#if 1
			// xy11
			inv_window[0] = _mm_setr_ps(wx4.f[0], wy, 1, 1);
			inv_window[1] = _mm_setr_ps(wx4.f[1], wy, 1, 1);
			inv_window[2] = _mm_setr_ps(wx4.f[2], wy, 1, 1);
			inv_window[3] = _mm_setr_ps(wx4.f[3], wy, 1, 1);
			
			// Multiply by the weights and the inv window factor
			pixelsf[0].sse = _mm_mul_ps(_mm_mul_ps(pixelsums[0], factor), inv_window[0]);
			pixelsf[1].sse = _mm_mul_ps(_mm_mul_ps(pixelsums[1], factor), inv_window[1]);
			pixelsf[2].sse = _mm_mul_ps(_mm_mul_ps(pixelsums[2], factor), inv_window[2]);
			pixelsf[3].sse = _mm_mul_ps(_mm_mul_ps(pixelsums[3], factor), inv_window[3]);
#else
			// No inv factor, a good bit faster
			pixelsf[0].sse = _mm_mul_ps(pixelsums[0], factor);
			pixelsf[1].sse = _mm_mul_ps(pixelsums[1], factor);
			pixelsf[2].sse = _mm_mul_ps(pixelsums[2], factor);
			pixelsf[3].sse = _mm_mul_ps(pixelsums[3], factor);
#endif
			
			// Finally, clamp
			// You can disable clamping for some cool colours from the integer overflow
			pixelsf[0].sse = _mm_clamp(pixelsf[0].sse, zero, ff);
			pixelsf[1].sse = _mm_clamp(pixelsf[1].sse, zero, ff);
			pixelsf[2].sse = _mm_clamp(pixelsf[2].sse, zero, ff);
			pixelsf[3].sse = _mm_clamp(pixelsf[3].sse, zero, ff);
			
			// Each pixelsf register corresponds to a single pixel (rgba)
			u32 px0 = pack_pixel_bgra(pixelsf[0].f[0], pixelsf[0].f[1], pixelsf[0].f[2], 255);
			u32 px1 = pack_pixel_bgra(pixelsf[1].f[0], pixelsf[1].f[1], pixelsf[1].f[2], 255);
			u32 px2 = pack_pixel_bgra(pixelsf[2].f[0], pixelsf[2].f[1], pixelsf[2].f[2], 255);
			u32 px3 = pack_pixel_bgra(pixelsf[3].f[0], pixelsf[3].f[1], pixelsf[3].f[2], 255);
			
			__m128i pixelsi = _mm_setr_epi32(px0, px1, px2, px3);
			pixels128[p] = pixelsi;
			p++;
		}
	}
	TextureUpdateAndRender(&Screen);
}

#undef _mm_square
#undef _mm_clamp

#else
#define RenderWithSoftwareSSE2 RenderWithSoftware
#endif /* NO_SSE2_RENDERER, top of RenderWithSoftwareSSE2 */


// The original software rendered verion :)
void RenderWithSoftware()
{
	if(SoftEarlyReturn())
	{
		return;
	}
	
	const int width = Screen.width;
	const int height = Screen.height;
	
	u32 *pixels = Screen.pixels;
	int p = 0;
	
	float dy2[MAX_METABALL_COUNT];
	for(int y = 0; y < height; y++)
	{
		
		// Modulate red and green by the pixel position
		float wy = Uniforms.inv_WindowHeight * y;
		
		// Precalculate dy squared
		// It doesn't seem to make a difference in speed on my machine though
		for(int b = 0; b < Uniforms.MetaballCount; b++)
		{
			float dy = y - metaballs[b].y;
			dy2[b] = dy * dy;
		}
		for(int x = 0; x < width; x++)
		{
			float sum = 0;
			for(int b = 0; b < Uniforms.MetaballCount; b++)
			{
				// float s = calc_metaball(metaballs[b].x, metaballs[b].y, x, y, metaballs[b].r);
				float s = calc_metaball_soft(metaballs[b].x, x, dy2[b], metaballs[b].r);
				sum += s;
			}
			
			// Modulate red and green by the pixel position
			float wx = Uniforms.inv_WindowWidth * x;
			
			// Note: sum is unclamped and overflows the colors, but this is intended
			// The value  is "saturated" afterwards to match the glsl version
			// That is, the multiplication is calculated first with an unbounded sum first, and bounded only after
			// Hint: Remove wx and wy for a constant colour
#if 1
			u32 r = Min(sum * ((255.0f * 0.8f) * wx), 255.0f);
			u32 g = Min(sum * ((255.0f * 0.5f) * wy), 255.0f);
			u32 b = Min(sum * (255.0f * 0.4f), 255.0f);
			u32 a = 255;
#else
			// Unclamped (psychedelic colours from integer overflow)
			// Also a constant background color
			// It's a lot easier to see the charateristic metaball behaviour in this mode
			// Note: Won't match the glsl shader
			u32 r =sum * (255.0f * 0.8f);
			u32 g =sum * (255.0f * 0.5f);
			u32 b =sum * (255.0f * 0.4f);
			u32 a = 255;
#endif
			u32 color = PACK_BGRA(r, g, b, a);
			pixels[p] = color;
			p++;
		}
	}
	TextureUpdateAndRender(&Screen);
}

void R_PrepareRenderMode()
{
	switch(r.RenderMode)
	{
		case R_HW:
			glUseProgram(r.Shader);
			glDisable(GL_TEXTURE_2D);
			
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
		break;
		
		case R_SW:
		case R_SW_SLOW:
			glUseProgram(0);
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, Screen.id);
			
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0, 1, 1, 0, 0, 1);
			
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
		break;
	}
}

const char *R_GetRenderModeString()
{
	switch(r.RenderMode)
	{
		case R_HW: return "Hardware rendering";
		case R_SW: return "Software rendering";
		case R_SW_SLOW: return "Software rendering";
	}
	return "Unknown rendering mode";
}

void UpdateMetaballs(float dt, float timestamp)
{
	if(SystemInfo.Paused) return;
	
	// Don't update the first one
	for(int i = 1; i < Uniforms.MetaballCount; i++)
	{
		metaball *m = &metaballs[i];
		MetaballUpdate(m, dt, timestamp);
	}
}

double SYS_GetElapsedSeconds()
{
	return (double)SDL_GetPerformanceCounter() / SystemInfo.PerformanceFrequency;
}

// Show some info about the cpu usage and rendering mode in the title bar
void UpdateWindowTitle(SDL_Window *Window, float ElapsedSeconds, int ForceUpdate)
{
	const float Frequency = 1;
	static float Accumulator = 1;
	float CpuUsage = SystemInfo.CpuUsage;
	Accumulator += ElapsedSeconds;
	if((Accumulator >= Frequency) || (ForceUpdate == 1))
	{
		static char title[256];
		const char *mode = R_GetRenderModeString();
		
		snprintf(title, sizeof(title), "Metaballs --- %.2f CPU used [%s]\n", CpuUsage, mode);
		SDL_SetWindowTitle(Window, title);
		
		Accumulator = 0;
	}
}

void DisplayHelp()
{
	if(ShowHelp)
	{
		CON_SetColor(1, 1, 1, 1);
		
		float x = 640 - 520;
		float y = 2;
		CON_BeginAt(x, y);
		
		float border = 4;
		CON_Box(x - border, 0, 640 - x + border, 135);
		
		CON_Printf("1:     Use hardware rendering");
#if NO_SSE2_RENDERER
		CON_Printf("2:     Use software rendering");
#else
		CON_Printf("2:     Use software rendering (sse2)");
#endif
		CON_Printf("3:     Use software rendering");
		CON_Printf("LMB:   Add a metaball");
		CON_Printf("RMB:   Remove a metaball");
		CON_Printf("SPACE: Toggle ball movement");
		CON_Printf("F:     Toggle fullscreen");
		CON_Printf("F2:    Show system info");
		CON_Printf("Q/ESC: Quit");
		CON_Printf("\n");
		CON_Printf("F1:    Show this help");
	}
}

void DisplaySystemInfo()
{
	if(ShowSystemInfo)
	{
		CON_SetColor(1, 1, 1, 1);
		
		float x = 640 - 425;
		CON_BeginAt(x, 2);
		
		float border = 4;
		CON_Box(x - border, 0, 640 - x + border, 75);
		
		CON_Printf("CPU Usage: %.2f%%", SystemInfo.CpuUsage);
		CON_Printf("%s", R_GetRenderModeString());
		CON_Printf("Metaball count: %d\n", Uniforms.MetaballCount);
		CON_Printf("Frame: %.2f ms", SystemInfo.FrameTime * 1000.0);
		CON_Printf("Render: %.2f ms", SystemInfo.RenderTime * 1000.0);
		CON_Printf("Paused: %s", SystemInfo.Paused ? "1" : "0");
	}
}

typedef enum
{
	A_COUNT,
	A_USE_HARDWARE,
	A_USE_SOFTWARE,
	A_LAST,
} arg_id;
// Argument parsing info
opt CommandlineArgs[] =
{
	[A_COUNT] = {OPTION("count", "c"), true, CONST_STR("COUNT"), CONST_STR("Start the program with COUNT metaballs")},
	[A_USE_HARDWARE] = {OPTION("hardware", "w"), false, CONST_STR(""), CONST_STR("Start the program with hardware rendering enabled")},
	[A_USE_SOFTWARE] = {OPTION("software", "s"), false, CONST_STR(""), CONST_STR("Start the program with software rendering enabled")},
	[A_LAST] = AP_LAST_ARG,
};

// Note: Win32 specific
// If OpenGL is using up all of your CPU in fullscreen mode, uncomment USE_WIN32_WINDOW_STYLE_HACK
// See wm.c for more details
// #define USE_WIN32_WINDOW_STYLE_HACK
#include "wm.c"

void GL_Finish()
{
	// On Windows, calling glFinish allows for more accurate frame timings
	// How it impacts performance in other configurations is unknown
	if(cfg.GlFinishEnabled)
	{
		glFinish();
	}
}

int main(int argc, char **argv)
{
	int DefaultMetaballCount = 5;
	render_mode DefaultRenderMode = R_HW;
	/**********************************
	* Parse options before anything
	**********************************/
	{
		ap_SetProgramName(argv[0]);
		ap_SetUsageString("[OPTION]...");
		int arg = 0;
		while((arg = ap_ParseOpts(argc, argv, &CommandlineArgs[0])) != -1)
		{
			switch(arg)
			{
				case A_COUNT:
				{
					string_view s = ap_OptArg();
					char *rem = 0;
					int n = strtol(s.data, &rem, 10);
					int failed = 0;
					if(n < 0) failed = 1;
					if(rem && *rem) failed = 1;
					if(failed)
					{
						printf("ERROR: Expected a positive number but got '%s'\n", s.data);
						ExitDemoWithCode(1);
					}
					else
					{
						if(n < MAX_METABALL_COUNT)
						{
							DefaultMetaballCount = n;
						}
						else
						{
							printf("ERROR: Count of %d exceeds max metaball count of %d\n", n, MAX_METABALL_COUNT);
							ExitDemoWithCode(1);
						}
					}
				}
				break;
				
				case A_USE_SOFTWARE:
					DefaultRenderMode = R_SW;
				break;
				
				case A_USE_HARDWARE:
					DefaultRenderMode = R_HW;
				break;
			}
		}
		
		switch(ap_LastError())
		{
			case AP_ERR_NONE:
			case AP_ERR_NO_COMMAND_LINE_ARGUMENTS:
			break;
			
			case AP_ERR_HELP_REQUESTED:
				ExitDemo();
			break;
			
			default:
				printf("ERROR: %s\n", ap_FailureReason());
				_BuiltinHelp(); // from argparse.c
				ExitDemoWithCode(1);
		}
	}
	
	
	/**********************************
	* Continue with setup as usual
	**********************************/
	
	SDL_Init(SDL_INIT_VIDEO);
	
	SDL_Window *Window = SDL_CreateWindow("metaballs-hw", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_OPENGL);
	assert(Window);
	
	SDL_ShowCursor(0);
	
	// Always start out centered
	SDL_WarpMouseInWindow(Window, WINDOW_WIDTH / 2.0f, WINDOW_HEIGHT / 2.0f);
	
	// Ignore mousemotion in SDL_PollEvent calls
	SDL_EventState(SDL_MOUSEMOTION, SDL_IGNORE);
	
	SDL_GLContext Context = SDL_GL_CreateContext(Window);
	SDL_GL_MakeCurrent(Window, Context);
	
	// If I somehow messed something up in my extension loader and it's not working on your system, 
	// replace this line with your preferred one. 
	// I typically use GLEW for my projects, but there are many options out there.
	GL_LoadExtensions(SDL_GL_GetProcAddress);
	// glewInit();
	
	SDL_GL_SetSwapInterval(1);
	
	WM_Init(Window);
	SYS_Init();
	CFG_Init();
	
	Screen = TextureCreate(WINDOW_WIDTH, WINDOW_HEIGHT, cfg.TextureFilter);
	R_Init(DefaultRenderMode);
	
	CON_Init(640, 480);
	CON_SetScale(2, 2);
	
	// Used to multiply the metaball color by the pixel position of the fragment being rendered
	Uniforms.inv_WindowWidth = 1.0 / (float)WINDOW_WIDTH;
	Uniforms.inv_WindowHeight = 1.0 / (float)WINDOW_HEIGHT;
	
	
	// User controlled
	metaballs[0].r = 40;
	
	// Static
	// This is a good one to test with
	// metaballs[1] = MetaballNew(300, 300, 40);
	
	// On my machine, a count of 20 just about hits the limit of keeping a framerate of 60 fps in the software renderer 
	// without going into SIMD optimizations
	// With the SIMD routine RenderWithSoftwareSSE2, I can render 95-100 of them at 60 fps
	
	// Note that the count `includes` the user controlled one, so you probably want at least 2 here
	Uniforms.MetaballCount = DefaultMetaballCount;
	
	// Parse options
	
	// Initialize all of the metaballs regardless of the count so that the user can add or subtract them during runtime
	// However, this prevents randomization
	// Todo: Implement randomization on adding?
	for(int i = 1; i < Uniforms.MetaballCount; i++)
	{
		metaballs[i] = MetaballNewRandomized();
	}
	
	
	// Init frame counter
	SystemInfo.PerformanceFrequency = SDL_GetPerformanceFrequency();
	double CurrentCounter;
	float FrameTime = 0.0016;
	CurrentCounter = SYS_GetElapsedSeconds();
	SystemInfo.StartTime = CurrentCounter;
	SystemInfo.GPU = (const char *)glGetString(GL_RENDERER);
	
	/**********************************
	* Main loop
	**********************************/
	while(Running)
	{
		SystemInfo.FrameTime = FrameTime;
		
		SDL_Event Event;
		while(SDL_PollEvent(&Event))
		{
			switch(Event.type)
			{
				case SDL_QUIT:
				ExitDemo();
				break;
				
				case SDL_MOUSEBUTTONDOWN:
				{
					switch(Event.button.button)
					{
						case SDL_BUTTON_LEFT:
						{
							// Always generate a random metaball
							// Alternatively, I can initialize everything once at startup and only change the uniform value
							int LastCount = Uniforms.MetaballCount;
							Uniforms.MetaballCount = Min(Uniforms.MetaballCount + 1, MAX_METABALL_COUNT);
							if(LastCount > 0 && LastCount < MAX_METABALL_COUNT)
							{
								metaballs[LastCount] = MetaballNewRandomized();
							}
						}
						break;
						
						case SDL_BUTTON_RIGHT:
						Uniforms.MetaballCount = Max(Uniforms.MetaballCount - 1, 0);
						if(Uniforms.MetaballCount == 0) r.ShouldClearSoftwareBuffer = 1;
						break;
					}
				}
				break;
				
				
				case SDL_KEYDOWN:
				{
					if(Event.key.repeat) break;
					
					int sym = Event.key.keysym.sym;
					switch(sym)
					{
						case SDLK_q:
						case SDLK_ESCAPE:
							ExitDemo();
						break;
						
						case SDLK_e:
							ProfileReport();
						break;
						
						case SDLK_r:
							ProfileClear();
						break;
						
						// Use hardware rendering
						case SDLK_1:
							r.RenderMode = R_HW;
							R_PrepareRenderMode();
							UpdateWindowTitle(Window, FrameTime, 1);
							printf("%s\n", R_GetRenderModeString());
							MSG_Send("%s", R_GetRenderModeString());
						break;
						
						// Use software rendering
						case SDLK_2:
						case SDLK_3:
							r.RenderMode = (sym == SDLK_2) ? R_SW : R_SW_SLOW;
							R_PrepareRenderMode();
							UpdateWindowTitle(Window, FrameTime, 1);
							printf("%s\n", R_GetRenderModeString());
							MSG_Send("%s", R_GetRenderModeString());
						break;
						
						// SystemInfo and Help are displayed in the same area,
						// so disable the one that's interfering
						case SDLK_F1:
							ShowHelp = !ShowHelp;
							ShowSystemInfo = 0;
						break;
						
						case SDLK_F2:
							ShowSystemInfo = !ShowSystemInfo;
							ShowHelp = 0;
						break;
						
						case SDLK_SPACE:
							SystemInfo.Paused = !SystemInfo.Paused;
						break;
						
						// Toggle fullscreen
						case SDLK_f:
						{
							SystemInfo.Fullscreen = !SystemInfo.Fullscreen;
							if(SystemInfo.Fullscreen)
							{
								SDL_SetWindowFullscreen(Window, SDL_WINDOW_FULLSCREEN_DESKTOP);
							}
							else
							{
								SDL_SetWindowFullscreen(Window, 0);
							}
						}
						break;
					}
				}
				break;
			}
		}
		
		// Always poll for mouse position
		int mx, my;
		SDL_GetMouseState(&mx, &my);
		
		// Always poll for window size
		int ww, wh;
		SDL_GetWindowSize(Window, &ww, &wh);
		r.ViewportWidth = ww;
		r.ViewportHeight = wh;
		R_Viewport();
		
		// Update user controlled metaball
		metaballs[0].x = mx;
		metaballs[0].y = my;
		
		// Update rest of the metaballs
		// Note: CurrentCounter is converted to milliseconds for more oscillation
		// Passing seconds doesn't work here
		UpdateMetaballs(FrameTime, CurrentCounter * 1000.0);
		
		GL_Finish();
		
		ProfileBegin("metaballs");
		
		switch(r.RenderMode)
		{
			case R_HW:
				RenderWithHardware();
			break;
			case R_SW:
				RenderWithSoftwareSSE2();
			break;
			case R_SW_SLOW:
				RenderWithSoftware();
			break;
		}
		
		ProfileEnd();
		
		double RenderTimestamp = SYS_GetElapsedSeconds();
		
		DisplayHelp();
		DisplaySystemInfo();
		MSG_Update(FrameTime);
		CON_Flush();
		
		
		SystemInfo.CpuUsage = SYS_ProcessorUsage(FrameTime);
		UpdateWindowTitle(Window, FrameTime, 0);
		
		SDL_GL_SwapWindow(Window);
		
		double LastCounter = CurrentCounter;
		CurrentCounter = SYS_GetElapsedSeconds();
		FrameTime = CurrentCounter - LastCounter;
		SystemInfo.RenderTime = RenderTimestamp - LastCounter;
		SystemInfo.RunningTime = CurrentCounter - SystemInfo.StartTime;
	}

	return 0;
}
