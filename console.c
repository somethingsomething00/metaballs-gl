// Simple font rendering api
// Uses OpenGL 1.2 as a backend, but also tries to play nice with shader programs if they exist
// This api is intended to print single line statements per call to CON_Print or CON_Printf
// Newlines are valid, but maybe not be coloured correctly


#include <stdio.h>
#include <stdarg.h>
#include <assert.h>

#include <GL/gl.h>

#ifndef GL_CURRENT_PROGRAM
  #define GL_CURRENT_PROGRAM 0x8B8D
#endif

#include "stb_easy_font.h"


#define CON_ADVANCE 12
#define CON_ROW_COUNT 1024

typedef struct
{
	float x;
	float y;
	float z;
	unsigned char color[4];
} easy_font_vert;


#define CON_VERT_COUNT (8192 * 2)
#define CON_BOX_COUNT 16
static easy_font_vert ConVerts[CON_VERT_COUNT];


// Set 1 color per line to avoid extra calls to glColor4f
typedef struct
{
	easy_font_vert *VertPointer;
	int VertCount;
	float color[4];
} console_row;

typedef struct
{
	float x;
	float y;
	float w;
	float h;
} console_box;

static console_row ConRows[CON_ROW_COUNT];
static console_box ConBoxes[CON_BOX_COUNT];

typedef struct 
{
	int OrthoWidth;
	int OrthoHeight;
	int VertsUsed;

	float OriginX;
	float OriginY;

	float CursorX;
	float CursorY;

	float Color[4];

	float ScaleX;
	float ScaleY;

	FILE *FileHandle;
	int EchoToFileHandle;

	int RowsUsed;

	int BoxesUsed;

} console_state;

static console_state ConState = {0};

void CON_Ortho(int width, int height)
{
	ConState.OrthoWidth = width;
	ConState.OrthoHeight = height;
}

void CON_SetColor(float r, float g, float b, float a)
{
	ConState.Color[0] = r;
	ConState.Color[1] = g;
	ConState.Color[2] = b;
	ConState.Color[3] = a;
}

void CON_SetScale(float x, float y)
{
	ConState.ScaleX = x;
	ConState.ScaleY = y;
}


void CON_SetOrigin(float x, float y)
{
	ConState.OriginX = x;
	ConState.OriginY = y;
}

void CON_SetFileHandle(FILE *f)
{
	ConState.FileHandle = f;
}

void CON_EnableEchoToFileHandle(int enable)
{
	ConState.EchoToFileHandle = enable;
}

void CON_Init(int width, int height)
{
	console_state Empty = {0};
	ConState = Empty;

	// Some sane defaults
	CON_Ortho(width, height);
	CON_SetScale(1, 1);
	CON_SetColor(1, 1, 1, 1);
	CON_SetOrigin(0, 0);

	CON_SetFileHandle(stdout);
	CON_EnableEchoToFileHandle(0);

	stb_easy_font_spacing(0);
}

void CON_Begin()
{
	ConState.CursorX = ConState.OriginX;
	ConState.CursorY = ConState.OriginY;
}

void CON_BeginAt(float x, float y)
{
	CON_SetOrigin(x, y);
	CON_Begin();
}

// Submit a text buffer without the use of va_list
// This is the one that does all the work
void CON_Print(const char *buf, size_t len)
{
	float x = ConState.CursorX;
	float y = ConState.CursorY;
	int BufSize = CON_VERT_COUNT - ConState.VertsUsed;
	assert(BufSize > 0);
	int QuadCount = stb_easy_font_print(x, y, (char *)buf, 0, &ConVerts[ConState.VertsUsed], BufSize);

	int VertCount = QuadCount * 4;

	assert(ConState.RowsUsed < CON_ROW_COUNT);

	// Update row buffer
	// This allows for per-row colouring while also saving on calls to glColor4f
	console_row *Row = &ConRows[ConState.RowsUsed];
	Row->VertPointer = &ConVerts[ConState.VertsUsed];
	Row->VertCount = VertCount;
	Row->color[0] = ConState.Color[0];
	Row->color[1] = ConState.Color[1];
	Row->color[2] = ConState.Color[2];
	Row->color[3] = ConState.Color[3];

	ConState.RowsUsed++;
	

	// Update vertex buffer
	ConState.VertsUsed += VertCount;
	ConState.CursorY += CON_ADVANCE;

	if(ConState.FileHandle && ConState.EchoToFileHandle)
	{
		FILE *handle = ConState.FileHandle;
		fwrite(buf, 1, len, handle);
		
		// Add a newline for for a better output look
		if(len > 0)
		{
			int c = buf[len - 1];
			if(c != '\n') putchar('\n');
		}
		
		fflush(handle);
	}
}

// Formatted string api
void CON_Printf(const char *s, ...)
{
	static char buf[2048];
	va_list vl;
	
	va_start(vl, s);
	int written = vsnprintf(buf, sizeof(buf), s, vl);
	va_end(vl);

	CON_Print(buf, written);
}




void CON_Flush()
{
	if(ConState.VertsUsed <= 0) return;

	int LastProgram = 0;
	glGetIntegerv(GL_CURRENT_PROGRAM, &LastProgram);
	glUseProgram(0);

	// Save the matrix and attribute stack
	glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT | GL_TRANSFORM_BIT);

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, ConState.OrthoWidth, ConState.OrthoHeight, 0, 0, 1);
	glScalef(ConState.ScaleX, ConState.ScaleY, 1);

	// Boxes first
	glBegin(GL_QUADS);
	glColor4f(0, 0, 0, 0.35);
	for(int i = 0; i < ConState.BoxesUsed; i++)
	{
		console_box *b = &ConBoxes[i];
		float x0 = b->x;
		float x1 = b->x + b->w;
		float y0 = b->y;
		float y1 = b->y + b->h;

		glVertex2f(x0, y0);
		glVertex2f(x0, y1);
		glVertex2f(x1, y1);
		glVertex2f(x1, y0);
	}
	glEnd();



	// Speed up rendering a bit by submitting 1 quad at a time instead of 1 vertex at a time
	// Todo: glDrawArrays 
	assert(ConState.VertsUsed % 4  == 0);
	
	for(int i = 0; i < ConState.RowsUsed; i++)
	{
		easy_font_vert *vert = ConRows[i].VertPointer;
		int count = ConRows[i].VertCount;
		float *c = ConRows[i].color;
		glColor4f(c[0], c[1], c[2], c[3]);
		glBegin(GL_QUADS);
		for(int v = 0; v < count; v += 4)
		{
			glVertex2f(vert[v + 0].x, vert[v + 0].y);
			glVertex2f(vert[v + 1].x, vert[v + 1].y);
			glVertex2f(vert[v + 2].x, vert[v + 2].y);
			glVertex2f(vert[v + 3].x, vert[v + 3].y);
		}
		glEnd();
	}



	ConState.VertsUsed = 0;
	ConState.RowsUsed = 0;
	ConState.BoxesUsed = 0;




	// Restore the matrix and attribute stack
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	// This also restores the current matrix mode
	glPopAttrib();

	glUseProgram(LastProgram);
}

void CON_Box(float x, float y, float w, float h)
{
	if(ConState.BoxesUsed < CON_BOX_COUNT)
	{
		console_box b;
		b.x = x;
		b.y = y;
		b.w = w;
		b.h = h;
		
		ConBoxes[ConState.BoxesUsed++] = b;
	}
}

void CON_End()
{
	// Nothing specific for now
}
