#define XS_IMPLEMENTATION
#include "xorshift.h"


// For generation
xs_state32 random_state1 = {1234};

// For runtime
xs_state32 random_state2 = {4567};

float random_f32_zero_to_one()
{
	return xs_f32(&random_state1);
}

float random_f32_range(float lo, float hi)
{
	return xs_range_in_f32(&random_state1, lo, hi);
}

