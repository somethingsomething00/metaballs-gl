#ifndef _CONFIG_H_
#define _CONFIG_H_

// Set this to 1 to disable the SSE2 software renderer
// If disabled, the code path will just use the default software renderer
#ifndef NO_SSE2_RENDERER
#define NO_SSE2_RENDERER 0
#endif

#endif /* _CONFIG_H_ */
